function [topMatch_ovlpgt_scores] = updateWeights_HDF5_train_Dataset(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%

recreateDataset = 1;
shuffleBatch = 1;
%%
fs = filesep;
netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
hdfPath = [netDataPath 'hdf5/'];
targetClassName = 'aeroplane';
if ~exist('winSizeList','var')
    winSizeList = [35];%[7 13 19];
end;
posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
posTrainHFilName = [posNegPatchPath 'trainPosPatches_35x35.h5'];
negTrainHFilName = [posNegPatchPath 'trainNegPatches_35x35.h5'];
posInfo = h5info(posTrainHFilName);
negInfo = h5info(negTrainHFilName);
posPatchNum = posInfo.Datasets(1).Dataspace.Size(4);
negPatchNum = negInfo.Datasets(1).Dataspace.Size(4);
patchSize = posInfo.Datasets(1).Dataspace.Size(1:3);
batchSize = 100; % predefined
batPosNum = 40;
batNegNum = 60;
batchNum = ceil(negPatchNum/batNegNum); % you can predefine too
dataSubsetNum = 15;
batchNumPerSubset = floor(batchNum/dataSubsetNum);
patchNumPerSubset = batchNumPerSubset * batchSize;
subwinSize = patchSize(1);
trainHDFFolder = sprintf('%strain_batch_%dx%d/',hdfPath,subwinSize,subwinSize); if ~isdir(trainHDFFolder) mkdir(trainHDFFolder); end;
testHDFFolder = sprintf('%stest_batch_%dx%d/',hdfPath,subwinSize,subwinSize); if ~isdir(testHDFFolder) mkdir(testHDFFolder); end;
%%
posLabel = 1; 
negLabel = 0;
posSampleWeight = single(1);
negSampleWeight = (batPosNum/batNegNum);
%%
curPosPatchIdx = 1;
curNegPatchIdx = 1;
%%% repetitive
posLabels = posLabel * ones(batPosNum,1);
posWeights = posSampleWeight * ones(batPosNum,1);
negLabels = negLabel * ones(batNegNum,1);
negWeights = negSampleWeight * ones(batNegNum,1);
%%
for dd = 1 : dataSubsetNum
    %% Create HDF5 dataset
    trainDatasetFileName = sprintf('%strainHDF_%d_%dx%d.h5',trainHDFFolder, dd, subwinSize, subwinSize);
    subsetInfo = h5info(trainDatasetFileName);
    subsetDataNum = subsetInfo.Datasets(1).Dataspace.Size(4);
    subsetLabels = h5read(trainDatasetFileName,'/label', 1, subsetDataNum);
    newWeights = zeros(subsetDataNum, 1);
    newWeights(find(subsetLabels==posLabel)) = posSampleWeight;
    newWeights(find(subsetLabels==negLabel)) = negSampleWeight;
    h5write(trainDatasetFileName,'/sample_weight',single(newWeights), 1, subsetDataNum);
end; % dd

