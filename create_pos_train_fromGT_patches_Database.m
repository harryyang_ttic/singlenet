function [topMatch_ovlpgt_scores] = create_pos_patches_Database(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
regetPosTrainData = 1;
regetNegTrainData = 0;
%%
fs = filesep;
netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
sampleDataPath = [netDataPath 'samples/'];
targetClassName = 'aeroplane';
if ~exist('winSizeList','var')
    %     winSizeList = [3 5 7 9 11 13 15 17 19 21 23 25 27];
    winSizeList = [35];%[7 13 19];
    % winSizeList = [3 7 11 15 19 23 27];
end;
if ~exist('nsampPos','var')
    nsampPos = 20; end;
savePosTrainPreFileName = ['SingleNetTrainPos_' targetClassName '_' num2str(nsampPos) '.mat'];%['VOCShapeTrainSet_' targetClassName '_' num2str(nsampPos) '.mat'];
% saveNegTrainFileName = ['SingleNetTrainNeg_' targetClassName '_' num2str(nsampPos) '.mat'];
realSegPath = '/share/project/shapes/data/segment_results/voc12berkeley/';
codePath = '/share/project/shapes/codes/'; addpath(codePath);
[vocPath, imgPath, gtPath, imgSetPath] = VOCDataPaths();
posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
saveTrainPrefix = 'trainHDF';
minMaskPixThrsh = 50; % min size of mask should be 100

%%
% dsInfo = mapDataSets('voc12','fg-all',targetClassName);
addpath('/share/project/shapes/libs/shape_context/');
addpath('/share/project/shapes/vocdevkit/VOCcode');

VOCinit;
%[extTrainFileList,gt]=textread(sprintf(VOCopts.imgsetpath,['../Segmentation/train']),'%s %d');
[extTrainFullList, extTrainFileList, extDatasetNames, sbdPath] = getExpandTrainList_VOC();
sbdInstPath = [sbdPath 'inst/']; sbdClsPath = [sbdPath 'cls/'];
extTrainImgNum = length(extTrainFileList);
[valList,gt]=textread(sprintf(VOCopts.imgsetpath,['../Segmentation/val']),'%s %d');
valImgNum = length(valList);
classNameList = VOCopts.classes;
targetClassLabel = find(strcmp(classNameList,targetClassName));
close all;


saveScoreFolder = '../data/cache_voc2012/';
%load([netDataPath saveNegTrainFileName],'negData','negInstMasks','negInstImages','negInstGTMasks','maskW','maskH');

if ~exist('beginNum','var')
    beginNum = 1; end;
if ~exist('endNum','var')
    endNum = 1; end;
if ~exist('distortNum','var')
    distortNum = 10; % generate distorted query image
end;

%%%Define flags and parameters:
%%
winSizeNum = length(winSizeList);
tau = 0.7
%%
% trainPosInstNum = length(posInstMasks);
queryNum = endNum - beginNum + 1;% length(testSet);
pixValThsh = 0.5;
r=1; % annealing rate
w=4;
sf=1;

%% Parse Vararin
for pair = reshape(varargin,2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp']);
    clear tmp;
end; %pair
checkDir(saveScoreFolder);
%%  Precompute Training Images Patches

%% Query Image
%% A (undistorted) query image
topMatch_ovlpgt_scores = zeros(1,winSizeNum);
% topMatchWins_ovlpScores = zeros(distortNum, winSizeNum, queryNum);
% topMatchWins_Idx = zeros(distortNum, winSizeNum, queryNum);
allData = struct('queryImage',[],'distortions',[]);
dist_ovlp_gt = cell(1,queryNum);
%% Padding
dx = ceil(max(winSizeList) /2);
dy = dx;
padr = ceil(max(winSizeList) /2)+dx;
if regetPosTrainData == 1
    disp(['Loading ' [sampleDataPath savePosTrainPreFileName]]);
    tic; load([sampleDataPath savePosTrainPreFileName],'nsampPos','posData','posInstMasks','posInstImages','maskW','maskH');toc;
    trainPosInstNum = length(posInstMasks);
    for pp = 1 : length(posInstMasks)
        posInstMasks{pp} = padarray(posInstMasks{pp},[padr padr]);
        posInstImages{pp} = padarray(posInstImages{pp},[padr padr],'symmetric');
    end;
else
    tic;load([sampleDataPath savePosTrainPreFileName],'trainPosInstNum');toc;
end;
%% get windows around sample points
for ww = 1 : length(winSizeList)
    subwinSize = winSizeList(ww); % sxs window around sample points
    %     hdfPosPath = sprintf('%spos_batch_%dx%d/',posNegPatchPath,subwinSize,subwinSize);if ~isdir(hdfPosPath) mkdir(hdfPosPath); end;
    %     hdfNegPath = sprintf('%sneg_batch_%dx%d/',posNegPatchPath,subwinSize,subwinSize);if ~isdir(hdfNegPath) mkdir(hdfNegPath); end;
    disp(['---- Window Size ' num2str(subwinSize)]);
    winR = floor(subwinSize/2);
    %% Get Data Index
    %saveTrainPosPrefix = sprintf('%s%s_pos_%dx%d',hdfPosPath,saveTrainPrefix,subwinSize,subwinSize);
    %saveTrainNegPrefix = sprintf('%s%s_neg_%dx%d',hdfNegPath,saveTrainPrefix,subwinSize,subwinSize);
    totalPosPatchNum = trainPosInstNum * nsampPos * numel([-dx : dx: dx]) * numel([-dy : dy: dy]);
    %     totalPatchNum = totalPosPatchNum + totalNegPatchNum;
    posRandIdx = randperm(totalPosPatchNum);
    %     negRandIdx = randperm(totalNegPatchNum)+totalPosPatchNum;
    %     caffeBatchSize = 100; caffeBatchNum = ceil(totalPatchNum / caffeBatchSize);
    %     posNum = floor(caffeBatchSize * totalPosPatchNum/totalPatchNum);
    %     negNum = caffeBatchSize - posNum;
    %     allRandIdx = [];
    %     negBatchBeginNum = 1;
    %     for bid = 1 : caffeBatchNum
    %         posBatchBeginNum = posNum * (bid-1) + 1;
    %         posBatchEndNum = min(posNum * bid, totalPosPatchNum);
    %         curBatchPosNum = posBatchEndNum - posBatchBeginNum + 1;
    %         negBatchEndNum = min(negBatchBeginNum + (caffeBatchSize - curBatchPosNum)-1,totalNegPatchNum);
    %         curBatchNegNum = negBatchEndNum - negBatchBeginNum + 1;
    %         curBatchSize = curBatchPosNum + curBatchNegNum;
    %         curBatchIdx = [posRandIdx(posBatchBeginNum:posBatchEndNum) negRandIdx(negBatchBeginNum:negBatchEndNum)];
    %         curBatchIdx = curBatchIdx(randperm(curBatchSize));
    %         allRandIdx = [allRandIdx curBatchIdx];
    %         negBatchBeginNum = negBatchBeginNum + curBatchNegNum;
    %     end; % bid
    %% Create HDF5 dataset
    trainPosDatasetFileName = sprintf('%strainPosPatches_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
    if ~exist(trainPosDatasetFileName,'file')
        h5create(trainPosDatasetFileName,'/data',[subwinSize subwinSize 4 totalPosPatchNum],'Datatype','single');
        h5create(trainPosDatasetFileName,'/label',totalPosPatchNum,'Datatype','single');
        h5create(trainPosDatasetFileName,'/sample_weight',totalPosPatchNum,'Datatype','single');
    end;
    posSampleWeight = single(1);
    %     negSampleWeight = single(totalPosPatchNum / totalNegPatchNum);
    posLabel = single(1);
    negLabel = single(0);
    curPatchIdx = 0;
    %% Positive Train Data
    if regetPosTrainData == 1
        for pp = 1 : trainPosInstNum
            fprintf('%d started!',pp); tic;
            trainInstMask = posInstMasks{pp};
            orgInstImg = posInstImages{pp};
            trainInstMask = double(trainInstMask>pixValThsh);
            orgInstMaskImg = orgInstImg.*repmat(trainInstMask,[1 1 3]);
            [sr1 sr2] = size(trainInstMask);
            refPoints = posData.points(find(posData.imageIdx==pp),:);
            %% because of padding
            %% in create trainset, already first padding then sample so no need to change coord
            refPoints = refPoints + padr;
            %%
            xre = refPoints(:,1); yre = refPoints(:,2);
            for ii = 1 : nsampPos
                %% Translation
                for tx = -dx : dx: dx % translation x
                    for ty = -dy : dy: dy
                        trainPatchMask = trainInstMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
                        trainPatchImg = orgInstImg( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR), : );
                        trainPatchImg(:,:,end+1) = trainPatchMask;
                        %% Write to HDF5
                        curPatchIdx = curPatchIdx + 1;
                        idxInH5 = find(posRandIdx == curPatchIdx);
                        h5write(trainPosDatasetFileName,'/data',single(trainPatchImg), [1 1 1 idxInH5], [size(trainPatchImg) 1]);
                        h5write(trainPosDatasetFileName,'/label',posLabel, idxInH5, 1);
                        h5write(trainPosDatasetFileName,'/sample_weight',posSampleWeight, idxInH5, 1);
                    end; % ty
                end; % tx
            end;
            fprintf('%d done!\n',pp); toc;
        end; % pp
    end; % if regetPosTrainData
    %% Negative data
    %     if regetNegTrainData == 1
    %         load([netDataPath savePosTrainPreFileName],'nsampPos','maskW','maskH');
    %         nsampNeg = nsampPos * 2;
    %         meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
    %         refMaskSize = [meanMH meanMW];
    %         for mm = 1 : extTrainImgNum
    %             imgFullName = extTrainFullList{mm};
    %             [imgDir imgName imgFormat] = fileparts(imgFullName);
    %             orgImage = im2double(imread(imgFullName));
    %             imgH = size(orgImage,1); imgW = size(orgImage,2);
    %             switch extDatasetNames{mm}
    %                 case 'sbd'
    %                     load([sbdInstPath imgName '.mat'],'GTinst');
    %                     load([sbdClsPath imgName '.mat'],'GTcls');
    %                     Sclass = GTcls.Segmentation;
    %                     Sobj = GTinst.Segmentation;
    %                     objInstIdx = unique(Sobj);
    %                     objInstIdx = setdiff(objInstIdx, 0);%
    %                     objInstIdx = setdiff(objInstIdx, 255);
    %                     imgClassIdxList = GTinst.Categories;
    %                     if ~ismember(targetClassLabel,imgClassIdxList) % not the target class
    %                         disp(['Image has no ' targetClassName ', continue!']);
    %                         continue;
    %                     end;
    %                 case 'voc12'
    %                     rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
    %                     imgClassList = {rec.objects.class};
    %                     objInstIdx = find(strcmp(imgClassList,targetClassName));
    %                     if length(objInstIdx)
    %                         [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
    %                         [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
    %                     else
    %                         disp(['Image has no ' targetClassName ', continue!']);
    %                         continue;
    %                     end;
    %             end;
    %             imgRealSegFolder = [realSegPath imgName fs];
    %             imgRealSegList = dir([imgRealSegFolder '*.png']);
    %             targetClassGTMask = double(Sclass == targetClassLabel);
    %             for rr = 1 : length(imgRealSegList)
    %                 fprintf('%s Starts\n',[imgRealSegList(rr).name]);
    %                 [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
    %                 realClsIdx = setdiff(unique(realSegImg),[0 255]);
    %                 for rii = 1 : length(realClsIdx)
    %                     if realClsIdx(rii) ~= targetClassLabel
    %                         %disp(['Instance is not ' targetClassName ', continue!']);
    %                         continue;
    %                     end;
    %                     realInstMask = double(realSegImg == realClsIdx(rii));
    %                     [r c v] = find(realInstMask);
    %                     if length(find(realInstMask)) < minMaskPixThrsh
    %                         disp('Mask Too Small, Dump!'); continue; end;
    %                     bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
    %                     %%%%%%%%%%%%%%%%%%%%%
    %                     realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
    %                     orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
    %                     realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
    %                     %%%%%%%%%%%%%%%%%%%%%
    %                     realInstMask = imresize(realInstMask,refMaskSize);
    %                     orgInstImg = imresize(orgInstImg,refMaskSize);
    %                     realInstGTMask = imresize(realInstGTMask,refMaskSize);
    %                     %%%%%%%%%%%%%%%%%%%%%
    %                     realInstMask = padarray(realInstMask,[padr padr]);
    %                     orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
    %                     realInstGTMask = padarray(realInstGTMask,[padr padr]);
    %                     %%%%%%%%%%%%%%%%%%%%%
    %                     %% sample points for patches
    %                     [xre,yre,tre]=bdry_extract_3(realInstMask);
    %                     nsamp2=length(xre);
    %                     if nsamp2>=nsampNeg
    %                         [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampNeg);
    %                     end
    %                     xre = round(xre); yre = round(yre);
    %                     %%% No need to padd: points are sampled on padded masks
    %                     %xre = xre + padr; yre = yre + padr;
    %                     %negData.points = [negData.points; realPoints];
    %                     %% Sample negative patches along false positive boundaries
    %                     ty = 0; tx = 0;
    %                     for ii = 1 : nsampNeg
    %                         %$% No Translation
    %                         for tx = -dx : dx: dx % translation x
    %                             for ty = -dy : dy: dy
    %                                 trainPatchMask = realInstMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
    %                                 trainPatchGTMask = realInstGTMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
    %                                 ovlpGT = length(find(trainPatchMask .* trainPatchGTMask)) / length( find(trainPatchMask + trainPatchGTMask));
    %                                 if ovlpGT > 0.3
    %                                     continue;
    %                                 end;
    %                                 trainPatchImg = orgInstImg( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR), : );
    %                                 trainPatchImg(:,:,end+1) = trainPatchMask;
    %                                 %% Write to HDF5
    %                                 curPatchIdx = curPatchIdx + 1;
    %                                 idxInH5 = find(allRandIdx == curPatchIdx);
    %                                 h5write(trainPosDatasetFileName,'/data',single(trainPatchImg), [1 1 1 curPatchIdx], [size(trainPatchImg) 1]);
    %                                 h5write(trainPosDatasetFileName,'/label',negLabel, curPatchIdx, 1);
    %                                 h5write(trainPosDatasetFileName,'/sample_weight',negSampleWeight, curPatchIdx, 1);
    %                             end; % ty
    %                         end; % tx
    %                     end;
    %                     disp(['Processed ' num2str(curPatchIdx) ' Data']);
    %                 end; % rii
    %             end;  % rr
    %         end; % mm
    %     end; % if regetNegTrainData
end; % ww
return;


%%
function [resBbox] = getResBbox(img, newSize,pixValThrsh)
[r c v] = find(img);
resBbox = img(min(r):max(r),min(c):max(c));
resBbox = imresize(resBbox, newSize);
resBbox = double(resBbox > pixValThrsh);