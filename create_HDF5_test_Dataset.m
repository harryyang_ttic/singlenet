function [topMatch_ovlpgt_scores] = create_HDF5_test_Dataset(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%

recreateDataset = 1;
shuffleBatch = 1;
%%
fs = filesep;
netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
hdfPath = [netDataPath 'hdf5/'];
targetClassName = 'aeroplane';
if ~exist('winSizeList','var')
    winSizeList = [35];%[7 13 19];
end;
posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
posTestHFilName = [posNegPatchPath 'testPosPatches_RealPos_35x35.h5'];
negTestHFilName = [posNegPatchPath 'testNegPatches_35x35.h5'];
posInfo = h5info(posTestHFilName);
negInfo = h5info(negTestHFilName);
posPatchNum = posInfo.Datasets(1).Dataspace.Size(4);
negPatchNum = negInfo.Datasets(1).Dataspace.Size(4);
patchSize = posInfo.Datasets(1).Dataspace.Size(1:3);
batchSize = 100; % predefined
batPosNum = 60;%40;
batNegNum = 40;%60;
batchNum = ceil(negPatchNum/batNegNum); % you can predefine too
dataSubsetNum = 1;%4;
batchNumPerSubset = floor(batchNum/dataSubsetNum);
patchNumPerSubset = batchNumPerSubset * batchSize;
subwinSize = patchSize(1);
testHDFFolder = sprintf('%stest_batch_%dx%d/',hdfPath,subwinSize,subwinSize); if ~isdir(testHDFFolder) mkdir(testHDFFolder); end;
testHDFFolder = sprintf('%stest_batch_%dx%d/',hdfPath,subwinSize,subwinSize); if ~isdir(testHDFFolder) mkdir(testHDFFolder); end;
%%
posLabel = 1; 
negLabel = 0;
posSampleWeight = single(1);
negSampleWeight = (posPatchNum/negPatchNum) /10;
%%
curPosPatchIdx = 1;
curNegPatchIdx = 1;
%%% repetitive
posLabels = posLabel * ones(batPosNum,1);
posWeights = posSampleWeight * ones(batPosNum,1);
negLabels = negLabel * ones(batNegNum,1);
negWeights = negSampleWeight * ones(batNegNum,1);
%%
for dd = 1 : dataSubsetNum
    %% Create HDF5 dataset
    testDatasetFileName = sprintf('%stestHDF_%d_%dx%d.h5',testHDFFolder, dd, subwinSize, subwinSize);
    disp(['Creating ' testDatasetFileName]);
    if ~exist(testDatasetFileName,'file') || recreateDataset
        if recreateDataset
            delete(testDatasetFileName); end;
        totalPatchNum = batchSize * batchNum;
        % hdfDataNum = totalPatchNum; % Inf
        h5create(testDatasetFileName,'/data',[subwinSize subwinSize 4 patchNumPerSubset],'Datatype','single');%,'ChunkSize',[subwinSize subwinSize 1 1]);
        h5create(testDatasetFileName,'/label',patchNumPerSubset,'Datatype','single');%,'ChunkSize',[1]);
        h5create(testDatasetFileName,'/sample_weight',patchNumPerSubset,'Datatype','single');%,'ChunkSize',[1]);
    end;
    

    for bid = 1 : batchNumPerSubset
        if mod(bid,100) == 1
            tic;
        end;
        posBatData = h5read(posTestHFilName,'/data', [1 1 1 curPosPatchIdx],[patchSize batPosNum]);
        
        
        negBatData = h5read(negTestHFilName,'/data', [1 1 1 curNegPatchIdx],[patchSize batNegNum]);
        
        
        batchLabels = single([posLabels; negLabels]);
        batchWeights = single([posWeights; negWeights]);
        batchData = zeros([patchSize batchSize]);
        batchData(:,:,:,1:batPosNum) = posBatData;
        batchData(:,:,:,(batPosNum+1):batchSize) = negBatData;
        batchData = single(batchData);
        
        if shuffleBatch
            batRandIdx = randperm(batchSize);
            batchData = batchData(:,:,:,batRandIdx);
            batchLabels = batchLabels(batRandIdx);
            batchWeights = batchWeights(batRandIdx);
        end;
        %%% Write to HDF5
        idxInDB = (bid - 1) * batchSize + 1;
        h5write(testDatasetFileName,'/data',single(batchData), [1 1 1 idxInDB], [patchSize batchSize]);
        h5write(testDatasetFileName,'/label',batchLabels, idxInDB, batchSize);
        h5write(testDatasetFileName,'/sample_weight',batchWeights, idxInDB, batchSize);
        %%%
        curPosPatchIdx = mod(curPosPatchIdx + batPosNum,posPatchNum);
        if curPosPatchIdx + batPosNum > posPatchNum
            curPosPatchIdx = posPatchNum - batPosNum +1;
        end;
        curNegPatchIdx = mod(curNegPatchIdx + batNegNum,negPatchNum);
        if curNegPatchIdx + batNegNum > negPatchNum
            curNegPatchIdx = negPatchNum - batNegNum +1;
        end;
        if mod(bid,100) == 0
            bt = toc;
            disp(['Batch ' num2str(bid) ' Done, elapsed time: ' num2str(bt) 's']);
        end;
    end;
end; % dd
