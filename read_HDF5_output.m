function [] = read_HDF5_output()
thrsh = 0.5;
hdfOutputPath = '/share/project/shapes/caffe-weighted-samples/examples/singleNet/output/';
hdfOutputName = 'v0.3_output.h5';
hdfOutputFile = [hdfOutputPath hdfOutputName];
hinfo = h5info(hdfOutputFile);
%%
saveResFolder = '/share/project/shapes/singleNet/plots/';
saveFileName = 'hdf_output_results.mat';
%%


batchNum = length(hinfo.Datasets) /2; % data and labels
outScores = [];
outLabels = [];
gtLabels = [];

for bb = 1 : batchNum
    batDataName = hinfo.Datasets(bb).Name;
    batData = h5read(hdfOutputFile,['/' batDataName],[1 1 1 1], hinfo.Datasets(bb).Dataspace.Size);
    batData = permute(batData, [4 3 1 2]);
    batScores = batData(:,2);
    outScores = [outScores; batScores;];
    outLabels = [outLabels;(batScores > thrsh)];
    %%
    batLabelName = hinfo.Datasets(bb+batchNum).Name;
    batLabels = h5read(hdfOutputFile,['/' batLabelName],[1 1 1 1], hinfo.Datasets(bb+batchNum).Dataspace.Size);
    batLabels = permute(batLabels, [4 3 1 2]);
    gtLabels = [gtLabels; batLabels];
end;
outLabels = single(outLabels);
%%
[C, order] = confusionmat(gtLabels,outLabels);
imagesc(C); pause(0.1);
acc = length(find(outLabels == gtLabels)) / length(gtLabels);
disp(['Accuracy = ' num2str(acc*100) '%']);
posNum = length(find(gtLabels==1));
negNum = length(find(gtLabels==0));
falsePosNum = length( find(outLabels(find(gtLabels == 0))==1));
falseNegNum = length( find(outLabels(find(gtLabels == 1))==0));
save([saveResFolder saveFileName],'gtLabels','outLabels','outScores','C','falsePosNum','falseNegNum');