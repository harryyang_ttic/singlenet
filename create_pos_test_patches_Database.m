function [topMatch_ovlpgt_scores] = create_pos_test_patches_Database(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
countPosSample = 1;
regetPosTestData = 1;
posOvlpThrsh = 0.8;
%%
fs = filesep;
netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
sampleDataPath = '/share/project/shapes/singleNet/samples/';
targetClassName = 'aeroplane';
if ~exist('winSizeList','var')
    %     winSizeList = [3 5 7 9 11 13 15 17 19 21 23 25 27];
    winSizeList = [35];%[7 13 19];
    % winSizeList = [3 7 11 15 19 23 27];
end;
if ~exist('nsampPos','var')
    nsampPos = 20; end;
savePosTrainPreFileName = ['SingleNetTrainPos_' targetClassName '_' num2str(nsampPos) '.mat'];%['VOCShapeTestSet_' targetClassName '_' num2str(nsampPos) '.mat'];
% savePosTestFileName = ['SingleNetTestPos_' targetClassName '_' num2str(nsampPos) '.mat'];
realSegPath = '/share/project/shapes/data/segment_results/voc12berkeley/';
codePath = '/share/project/shapes/codes/'; addpath(codePath);
[vocPath, imgPath, gtPath, imgSetPath] = VOCDataPaths();
posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
saveTestPrefix = 'testHDF';
minMaskPixThrsh = 50; % min size of mask should be 100

%%
% dsInfo = mapDataSets('voc12','fg-all',targetClassName);
addpath('/share/project/shapes/libs/shape_context/');
addpath('/share/project/shapes/vocdevkit/VOCcode');

VOCinit;
%% Image sets
dsInfo = mapDataSets('voc12','val','inst');
valList = dsInfo.imnames;
testImgNum = length(valList);
valImgNum = length(valList);

classNameList = VOCopts.classes;
targetClassLabel = find(strcmp(classNameList,targetClassName));
close all;


saveScoreFolder = '../data/cache_voc2012/';
%load([netDataPath savePosTestFileName],'posData','posInstMasks','posInstImages','posInstGTMasks','maskW','maskH');

if ~exist('beginNum','var')
    beginNum = 1; end;
if ~exist('endNum','var')
    endNum = 1; end;
if ~exist('distortNum','var')
    distortNum = 10; % generate distorted query image
end;

%%%Define flags and parameters:
%%
winSizeNum = length(winSizeList);
tau = 0.7
%%
% testPosInstNum = length(posInstMasks);
queryNum = endNum - beginNum + 1;% length(testSet);
pixValThsh = 0.5;
r=1; % annealing rate
w=4;
sf=1;

%% Parse Vararin
for pair = reshape(varargin,2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp']);
    clear tmp;
end; %pair
checkDir(saveScoreFolder);
%%  Precompute Testing Images Patches

%% Query Image
%% A (undistorted) query image
% topMatch_ovlpgt_scores = zeros(1,winSizeNum);
% % topMatchWins_ovlpScores = zeros(distortNum, winSizeNum, queryNum);
% % topMatchWins_Idx = zeros(distortNum, winSizeNum, queryNum);
% allData = struct('queryImage',[],'distortions',[]);
% dist_ovlp_gt = cell(1,queryNum);
dx = 0; % No displacement during testing %ceil(max(winSizeList) /2);
dy = 0; % No displacement during testing %dx;
padr = ceil(max(winSizeList) /2)+dx;
% if regetPosTestData == 1
%     disp(['Loading ' [netDataPath savePosTestPreFileName]]);
%     tic; load([netDataPath savePosTestPreFileName],'nsampPos','posData','posInstMasks','posInstImages','maskW','maskH');toc;
%     testPosInstNum = length(posInstMasks);
%     for pp = 1 : length(posInstMasks)
%         posInstMasks{pp} = padarray(posInstMasks{pp},[padr padr]);
%         posInstImages{pp} = padarray(posInstImages{pp},[padr padr],'symmetric');
%     end;
% else
%     tic;load([netDataPath savePosTestPreFileName],'testPosInstNum');toc;
% end;
%% get windows around sample points
for ww = 1 : length(winSizeList)
    subwinSize = winSizeList(ww); % sxs window around sample points
    %     hdfPosPath = sprintf('%spos_batch_%dx%d/',hdfPath,subwinSize,subwinSize);if ~isdir(hdfPosPath) mkdir(hdfPosPath); end;
    %     hdfPosPath = sprintf('%spos_batch_%dx%d/',hdfPath,subwinSize,subwinSize);if ~isdir(hdfPosPath) mkdir(hdfPosPath); end;
    disp(['---- Window Size ' num2str(subwinSize)]);
    winR = floor(subwinSize/2);
    %% Count Posative patche number
    posCountFileName = [sampleDataPath 'posTestCount.mat'];
    if countPosSample == 1
        predClassMaskNum = 0;
        curPosPatchNum = 0;
        load([sampleDataPath savePosTrainPreFileName],'nsampPos','maskW','maskH');
        nsampPos = nsampPos * 2;
        meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
        refMaskSize = [meanMH meanMW];
        for mm = 1 : valImgNum
            imgFullName = [dsInfo.imdir valList{mm} '.' dsInfo.extension];
            [imgDir imgName imgFormat] = fileparts(imgFullName);
            orgImage = im2double(imread(imgFullName));
            imgH = size(orgImage,1); imgW = size(orgImage,2);
            switch dsInfo.dataset
                case 'sbd'
                    load([sbdInstPath imgName '.mat'],'GTinst');
                    load([sbdClsPath imgName '.mat'],'GTcls');
                    Sclass = GTcls.Segmentation;
                    Sobj = GTinst.Segmentation;
                    objInstIdx = unique(Sobj);
                    objInstIdx = setdiff(objInstIdx, 0);%
                    objInstIdx = setdiff(objInstIdx, 255);
                    imgClassIdxList = GTinst.Categories;
                    if ~ismember(targetClassLabel,imgClassIdxList) % not the target class
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
                case 'voc12'
                    rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
                    imgClassList = {rec.objects.class};
                    objInstIdx = find(strcmp(imgClassList,targetClassName));
                    if length(objInstIdx)
                        [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                        [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
                    else
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
            end;
            imgRealSegFolder = [realSegPath imgName fs];
            imgRealSegList = dir([imgRealSegFolder '*.png']);
            targetClassGTMask = double(Sclass == targetClassLabel);
            for rr = 1 : length(imgRealSegList)
                fprintf('%s Starts\n',[imgRealSegList(rr).name]);
                [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
                realClsIdx = setdiff(unique(realSegImg),[0 255]);
                for rii = 1 : length(realClsIdx)
                    if realClsIdx(rii) ~= targetClassLabel
                        %disp(['Instance is not ' targetClassName ', continue!']);
                        continue;
                    end;
                    predClassMaskNum = predClassMaskNum + 1;
                    realInstMask = double(realSegImg == realClsIdx(rii));
                    [r c v] = find(realInstMask);
                    if length(find(realInstMask)) < minMaskPixThrsh
                        disp('Mask Too Small, Dump!'); continue; end;
                    bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
                    orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = imresize(realInstMask,refMaskSize);
                    orgInstImg = imresize(orgInstImg,refMaskSize);
                    realInstGTMask = imresize(realInstGTMask,refMaskSize);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = padarray(realInstMask,[padr padr]);
                    orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
                    realInstGTMask = padarray(realInstGTMask,[padr padr]);
                    %%%%%%%%%%%%%%%%%%%%%
                    %% sample points for patches
                    [xre,yre,tre]=bdry_extract_3(realInstMask);
                    nsamp2=length(xre);
                    if nsamp2>=nsampPos
                        [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampPos);
                    end
                    xre = round(xre); yre = round(yre);
                    %%% No need to padd: points are sampled on padded masks
                    %xre = xre + padr; yre = yre + padr;
                    %posData.points = [posData.points; realPoints];
                    %% Sample posative patches along false positive boundaries
                    for ii = 1 : nsampPos
                        %$% No Translation
                        testPatchMask = realInstMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        testPatchGTMask = realInstGTMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        ovlpGT = length(find(testPatchMask .* testPatchGTMask)) / length( find(testPatchMask + testPatchGTMask));
                        if ovlpGT < posOvlpThrsh
                            continue;
                        end;
                        curPosPatchNum = curPosPatchNum + 1;                        
                    end;
                    disp(['Processed ' num2str(curPosPatchNum) ' Data']);
                end; % rii
            end;  % rr
        end; % mm
        disp(['Total predicted ' targetClassName ' Instances:' num2str(predClassMaskNum)]);
        disp(['Total False Positive Patches: ' num2str(curPosPatchNum)]);
        totalPosPatchNum = curPosPatchNum;
        save(posCountFileName,'predClassMaskNum','totalPosPatchNum');
    else
        load(posCountFileName,'predClassMaskNum','totalPosPatchNum');
    end; % if regetPosTestData
    %% Get Data Index
    %saveTestPosPrefix = sprintf('%s%s_pos_%dx%d',hdfPosPath,saveTestPrefix,subwinSize,subwinSize);
    %saveTestPosPrefix = sprintf('%s%s_pos_%dx%d',hdfPosPath,saveTestPrefix,subwinSize,subwinSize);
    %     totalPosPatchNum = testPosInstNum * nsampPos * numel([-dx : dx: dx]) * numel([-dy : dy: dy]);
    %     totalPatchNum = totalPosPatchNum + totalPosPatchNum;
    %     posRandIdx = randperm(totalPosPatchNum);
    posRandIdx = randperm(totalPosPatchNum);%+totalPosPatchNum;
    %     caffeBatchSize = 100; caffeBatchNum = ceil(totalPatchNum / caffeBatchSize);
    %     posNum = floor(caffeBatchSize * totalPosPatchNum/totalPatchNum);
    %     posNum = caffeBatchSize - posNum;
    %     allRandIdx = [];
    %     posBatchBeginNum = 1;
    %     for bid = 1 : caffeBatchNum
    %         posBatchBeginNum = posNum * (bid-1) + 1;
    %         posBatchEndNum = min(posNum * bid, totalPosPatchNum);
    %         curBatchPosNum = posBatchEndNum - posBatchBeginNum + 1;
    %         posBatchEndNum = min(posBatchBeginNum + (caffeBatchSize - curBatchPosNum)-1,totalPosPatchNum);
    %         curBatchPosNum = posBatchEndNum - posBatchBeginNum + 1;
    %         curBatchSize = curBatchPosNum + curBatchPosNum;
    %         curBatchIdx = [posRandIdx(posBatchBeginNum:posBatchEndNum) posRandIdx(posBatchBeginNum:posBatchEndNum)];
    %         curBatchIdx = curBatchIdx(randperm(curBatchSize));
    %         allRandIdx = [allRandIdx curBatchIdx];
    %         posBatchBeginNum = posBatchBeginNum + curBatchPosNum;
    %     end; % bid
    %% Create HDF5 dataset
    testPosDatasetFileName = sprintf('%stestPosPatches_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
    if ~exist(testPosDatasetFileName,'file')
        h5create(testPosDatasetFileName,'/data',[subwinSize subwinSize 4 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 1 1]);
        h5create(testPosDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
        h5create(testPosDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
    end;
    % posSampleWeight = single(1);
    %% Test by Stephen 1/29/2015
    posSampleWeight = single(5/95/10);%single(totalPosPatchNum / (10 * totalPosPatchNum));
    %%
    % posSampleWeight = single(totalPosPatchNum / totalPosPatchNum);
    posLabel = single(1);
    posLabel = single(0);
    curPatchIdx = 0;
    %% Posative data
    visited = 0;
    debugFlag = 0;
    if regetPosTestData == 1
        load([sampleDataPath savePosTrainPreFileName],'nsampPos','maskW','maskH');
        nsampPos = nsampPos * 2;
        meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
        refMaskSize = [meanMH meanMW];
        for mm = 1 : valImgNum
            imgFullName = [dsInfo.imdir valList{mm} '.' dsInfo.extension];
            [imgDir imgName imgFormat] = fileparts(imgFullName);
            %% debug
            if debugFlag
                debugImgName = '2011_002993';
                if ~strcmp(imgName,debugImgName) && visited == 0
                    continue;
                elseif strcmp(imgName,debugImgName)
                    visited = 1;
                    curPatchIdx = 1986024;
                end;
            end;
            %%
            orgImage = im2double(imread(imgFullName));
            imgH = size(orgImage,1); imgW = size(orgImage,2);
            switch dsInfo.dataset
                case 'sbd'
                    load([sbdInstPath imgName '.mat'],'GTinst');
                    load([sbdClsPath imgName '.mat'],'GTcls');
                    Sclass = GTcls.Segmentation;
                    Sobj = GTinst.Segmentation;
                    objInstIdx = unique(Sobj);
                    objInstIdx = setdiff(objInstIdx, 0);%
                    objInstIdx = setdiff(objInstIdx, 255);
                    imgClassIdxList = GTinst.Categories;
                    if ~ismember(targetClassLabel,imgClassIdxList) % not the target class
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
                case 'voc12'
                    rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
                    imgClassList = {rec.objects.class};
                    objInstIdx = find(strcmp(imgClassList,targetClassName));
                    if length(objInstIdx)
                        [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                        [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
                    else
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
            end;
            imgRealSegFolder = [realSegPath imgName fs];
            imgRealSegList = dir([imgRealSegFolder '*.png']);
            targetClassGTMask = double(Sclass == targetClassLabel);
            for rr = 1 : length(imgRealSegList)
                %%
                if debugFlag && visited
                    if ~strcmp(imgRealSegList(rr).name,[debugImgName '_S0030.png'])
                        continue;
                    else
                        curPatchIdx = 269839; end;
                end;
                %%
                tic;
                fprintf('%s Starts\n',[imgRealSegList(rr).name]);
                [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
                realClsIdx = setdiff(unique(realSegImg),[0 255]);
                for rii = 1 : length(realClsIdx)
                    if realClsIdx(rii) ~= targetClassLabel
                        %disp(['Instance is not ' targetClassName ', continue!']);
                        continue;
                    end;
                    realInstMask = double(realSegImg == realClsIdx(rii));
                    [r c v] = find(realInstMask);
                    if length(find(realInstMask)) < minMaskPixThrsh
                        disp('Mask Too Small, Dump!'); continue; end;
                    bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
                    orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = imresize(realInstMask,refMaskSize);
                    orgInstImg = imresize(orgInstImg,refMaskSize);
                    realInstGTMask = imresize(realInstGTMask,refMaskSize);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = padarray(realInstMask,[padr padr]);
                    orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
                    realInstGTMask = padarray(realInstGTMask,[padr padr]);
                    %%%%%%%%%%%%%%%%%%%%%
                    %% sample points for patches
                    [xre,yre,tre]=bdry_extract_3(realInstMask);
                    nsamp2=length(xre);
                    if nsamp2>=nsampPos
                        [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampPos);
                    end
                    xre = round(xre); yre = round(yre);
                    %%% No need to padd: points are sampled on padded masks
                    %xre = xre + padr; yre = yre + padr;
                    %posData.points = [posData.points; realPoints];
                    %% Sample posative patches along false positive boundaries
                    for ii = 1 : nsampPos
                        %$% No Translation
                        testPatchMask = realInstMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        testPatchGTMask = realInstGTMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        ovlpGT = length(find(testPatchMask .* testPatchGTMask)) / length( find(testPatchMask + testPatchGTMask));
                        if ovlpGT < posOvlpThrsh
                            continue;
                        end;
                        testPatchImg = orgInstImg( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR), : );
                        testPatchImg(:,:,end+1) = testPatchMask;
                        %% Write to HDF5
                        curPatchIdx = curPatchIdx + 1;
                        idxInH5 = find(posRandIdx == curPatchIdx);
                        if isempty(idxInH5) % out of limit
                            info = hdf5info(testPosDatasetFileName);
                            idxInH5 = info.GroupHierarchy.Datasets(1).Dims(4) +1;
                        end;
                        h5write(testPosDatasetFileName,'/data',single(testPatchImg), [1 1 1 idxInH5], [size(testPatchImg) 1]);
                        h5write(testPosDatasetFileName,'/label',posLabel, idxInH5, 1);
                        h5write(testPosDatasetFileName,'/sample_weight',posSampleWeight, idxInH5, 1);
                    end;
                    disp(['Processed ' num2str(curPatchIdx) ' Data']);
                end; % rii
                rrTime = toc;
                disp([imgRealSegList(rr).name ' takes ' num2str(rrTime) 's']);
            end;  % rr
        end; % mm
    end; % if regetPosTestData    
end; % ww
return;


%%
function [resBbox] = getResBbox(img, newSize,pixValThrsh)
[r c v] = find(img);
resBbox = img(min(r):max(r),min(c):max(c));
resBbox = imresize(resBbox, newSize);
resBbox = double(resBbox > pixValThrsh);