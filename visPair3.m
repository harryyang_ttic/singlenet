function [fig] = visPair3(img1, win1,points1,i, ...
    img2, win2, points2, j, padSize, titleStrs)
if exist('img1','var') && exist('img2','var')
    plotrow = 2;
else
    plotrow = 1;
end;
% if ~exist('orgImgSize','var') orgImgSize = [28 28]; end;
    
% win1 = win1;%(i,:);
% win2 = win2;%(j,:);
% ws = sqrt(length(win1));
% wr = ws/2;
% win1 = reshape(win1,[ws ws]); win2 = reshape(win2,[ws ws]);
figure, subplot(plotrow,2,1); fig = gcf;
colormap gray
imagesc(win1); hold on; plot(wr,wr,'g*');axis image;
if exist('titleStrs','var')
    hold on; title(titleStrs{1}); hold off; end;
subplot(plotrow,2,2); imagesc(win2); hold on; plot(wr,wr,'r*');axis image;
if exist('titleStrs','var') && length(titleStrs) > 1
    hold on; title(titleStrs{2});  hold off;end

if plotrow == 2
    %%
    p1x = points1(i,1); p1y = points1(i,2);
    p2x = points2(j,1); p2y = points2(j,2);
    subplot(plotrow,2,3); imagesc(img1); hold on;
    if exist('padSize','var') % size(img1) > orgImgSize
        orgImgSize = (size(img1) - 2 * padSize);
        rectangle('Position',[padSize(2) padSize(1) orgImgSize(2) orgImgSize(1)],'EdgeColor','y','LineStyle','--'); end;
    plot(p1x,p1y,'g*');axis image;
    rectangle('Position',[p1x-wr p1y-wr ws ws],'EdgeColor','g');
    coord = sprintf('(%d, %d)',p1x, p1y); title(coord);
    hold off
    %%
    subplot(plotrow,2,4); imagesc(img2); hold on;
    if exist('padSize','var') % size(img2) > orgImgSize
        orgImgSize = (size(img2) - 2 * padSize);
        rectangle('Position',[padSize(2) padSize(1) orgImgSize(2) orgImgSize(1)],'EdgeColor','y','LineStyle','--'); end;    
    plot(p2x,p2y,'r*');axis image;
    rectangle('Position',[p2x-wr p2y-wr ws ws],'EdgeColor','r');
    coord = sprintf('(%d, %d)',p2x, p2y); title(coord);    
    hold off
end;
