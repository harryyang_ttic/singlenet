function [idx] = strSearch(strCell, target)
idxCell= strfind(strCell,target);
idx = find(not(cellfun('isempty',idxCell)));