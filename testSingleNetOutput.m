function [scores, maxlabel] = testSingleNetOutput(im, use_gpu)
%% Paths
codePath = '/share/project/shapes/codes/'; addpath(codePath);
caffePath = '/share/project/shapes/caffe-weighted-samples/';
matcaffePath = [caffePath 'matlab/caffe']; addpath(matcaffePath);
model_def_file = [caffePath 'examples/singleNet/test_v0.3.prototxt'];
model_file = [caffePath 'examples/singleNet/data/train_iter_318492.caffemodel'];
modelStr = textread(model_def_file,'%s','delimiter','\n');
saveResFolder = '/share/project/shapes/singleNet/plots/';
batchSize = strValSearch(modelStr, 'input_dim: %d');
batchSize = batchSize(1);
use_gpu = 1;
visualFlag = 1; close all;
subaxisParam = {'Spacing', 0.05, 'Padding', 0, 'Margin', 0.05};
colorRange = [0 5];
thresh = 0.5;
[p saveFileName1 f] = fileparts(model_def_file);
[p saveFileName2 f] = fileparts(model_file);
saveFileName = ['matlab_output_' saveFileName1 '_' saveFileName2];
%% init caffe network (spews logging info)
if exist('use_gpu', 'var')
    matcaffe_init(use_gpu,model_def_file,model_file);
else
    matcaffe_init(0,model_def_file,model_file);
end
%%
outLabels = [];
gtLabels = [];
outScores = [];
visualFlag = 1;
testDataPath = '/scratch/stephenchen/shapes/singleNet/hdf5/test_batch_35x35/';
testListFile = [caffePath 'examples/singleNet/testFileList.txt'];
testFileList = textread(testListFile,'%s','delimiter','\n');
for tt = 1 : length(testFileList)
    testDataFile = testFileList{tt};
    testInfo = h5info(testDataFile);
    testPatchNum = testInfo.Datasets(1).Dataspace.Size(4);
    patchSize = testInfo.Datasets(1).Dataspace.Size(1:3);
    labelSize = testInfo.Datasets(2).Dataspace.Size;
    batchNum = floor(testPatchNum/batchSize);
    for bb = 1 : batchNum
        tic;
        batchStart = (bb-1)*batchSize + 1;
        patchIdxInSubset = (bb-1)*batchSize + [1:batchSize];
        if max(patchIdxInSubset) > testPatchNum
            patchIdxInSubset = [ ((bb-1)*batchSize+1):testPatchNum];
        end;
        curBatSize = length(patchIdxInSubset);
        testData = h5read(testDataFile, '/data', [1 1 1 batchStart],[patchSize curBatSize]);
        %% 3/3/2015: Should NOT permute after read from HDF5!(Since the HDF Train/Test data did NOT permute([2 1 3 4]), therefore the test data will be transposedly used in Caffe
        %  Therefore batchData needs to be permute in Matlab before
        %  feeding into Caffe (while the data read from the
        %  unpermuted test_HDF.h5 doesn't
        % testData = permute(testData, [2 1 3 4]);
        %%
        inputData = {testData};
        testLabels = h5read(testDataFile, '/label', [batchStart],[curBatSize]);
        testWeights = h5read(testDataFile, '/sample_weight', [batchStart],[curBatSize]);
        % testLabel = permute(testLabel,[2 1 3 4]);
        tic;
        softmaxOut = caffe('forward', inputData);
        outProb = softmaxOut{1};
        outProb = outProb(:,:,2,:);
        outProb = permute(outProb, [4 1 2 3]);
        outScores = [outScores; outProb];
        batLabels = single(outProb > thresh);
        outLabels = [outLabels; batLabels;];
        gtLabels = [gtLabels; testLabels;];
        toc;
        if mod(bb,10) == 0
            disp(['Batch ' num2str(bb) ' Done']); end;
    end;  %bb
end; % tt
%%
[C, order] = confusionmat(gtLabels,outLabels);
if visualFlag % && mod(bb,10) == 0
    imagesc(C); pause(0.1);
    acc = length(find(outLabels == gtLabels)) / length(gtLabels);
    disp(['Accuracy = ' num2str(acc*100) '%']);
end;
posNum = length(find(gtLabels==1));
negNum = length(find(gtLabels==0));
falsePosNum = length( find(outLabels(find(gtLabels == 0))==1));
falseNegNum = length( find(outLabels(find(gtLabels == 1))==0));
save([saveResFolder saveFileName '_confMat.mat'],'gtLabels','outLabels','outScores','C','falsePosNum','falseNegNum');



