function [dataPath] = retGregDataPath()
[ret, hostname] = system('hostname');
if strfind(hostname,'poincare')
        dataPath = '/share/data/vision-greg/shapes/';
elseif strfind(hostname,'chenxistephen')
        dataPath = '/home/chenxistephen/vision-greg-data/';
end;