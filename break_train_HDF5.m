function [] = break_train_HDF5(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
getAllTrainData = 0;
countNegSample = 0;
regetPosTrainData = 1;
regetNegTrainData = 1;
%%
fs = filesep;
netPath = '/share/project/shapes/singleNet/'; % [pwd fs];
netDataPath = [netPath 'samples/'];
targetClassName = 'aeroplane';
if ~exist('winSizeList','var')
    %     winSizeList = [3 5 7 9 11 13 15 17 19 21 23 25 27];
    winSizeList = [35];%[7 13 19];
    % winSizeList = [3 7 11 15 19 23 27];
end;
if ~exist('nsampPos','var')
    nsampPos = 20; end;

hdfPath = [netPath 'hdf5/']; if ~isdir(hdfPath) mkdir(hdfPath); end;
saveTrainPrefix = 'trainHDF';
minMaskPixThrsh = 50; % min size of mask should be 100

%%
% dsInfo = mapDataSets('voc12','fg-all',targetClassName);
%[extTrainFileList,gt]=textread(sprintf(VOCopts.imgsetpath,['../Segmentation/train']),'%s %d');

close all;


saveScoreFolder = '../data/cache_voc2012/';
%load([netDataPath saveNegTrainFileName],'negData','negInstMasks','negInstImages','negInstGTMasks','maskW','maskH');

if ~exist('beginNum','var')
    beginNum = 1; end;
if ~exist('endNum','var')
    endNum = 1; end;
if ~exist('distortNum','var')
    distortNum = 10; % generate distorted query image
end;

%%%Define flags and parameters:
%%
winSizeNum = length(winSizeList);
tau = 0.7
%%
% trainPosInstNum = length(posInstMasks);
queryNum = endNum - beginNum + 1;% length(testSet);
pixValThsh = 0.5;
r=1; % annealing rate
w=4;
sf=1;

%% Parse Vararin
for pair = reshape(varargin,2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp']);
    clear tmp;
end; %pair
checkDir(saveScoreFolder);
%%  Precompute Training Images Patches

%% Padding
dx = ceil(max(winSizeList) /2);
dy = dx;
padr = ceil(max(winSizeList) /2)+dx;
%% get windows around sample points
localHDFPath = '/scratch/stephenchen/shapes/singleNet/hdf5/';
for ww = 1 : length(winSizeList)
    subwinSize = winSizeList(ww); % sxs window around sample points
    hdfTrainPath = sprintf('%strain_batch_%dx%d/',localHDFPath,subwinSize,subwinSize);if ~isdir(hdfTrainPath) mkdir(hdfTrainPath); end;
    hdfTestPath = sprintf('%stest_batch_%dx%d/',localHDFPath,subwinSize,subwinSize);if ~isdir(hdfTestPath) mkdir(hdfTestPath); end;
    disp(['---- Window Size ' num2str(subwinSize)]);
    winR = floor(subwinSize/2);
    %% 
    splitFileNum = 10;
     %% Create HDF5 dataset
    trainHDatasetFileName = sprintf('%strainHDF_%dx%d.h5',hdfPath, subwinSize, subwinSize);
    dsInfo = h5info(trainHDatasetFileName);
    dataSize = dsInfo.Datasets(1).Dataspace.Size;
    totalPatchNum = dataSize(4);
    patchSize = dataSize(1:3);
    splitFilePatchNum = ceil(totalPatchNum/splitFileNum);
    for ff = 1 : splitFileNum
        beginPatchId = (ff-1) * splitFilePatchNum + 1;
        endPatchId  = min( ff * splitFilePatchNum, totalPatchNum);
        curSplitPatchNum = endPatchId - beginPatchId + 1;
        splitHDatasetFileName = sprintf('%strainHDF_%d_%dx%d.h5',hdfTrainPath, ff, subwinSize, subwinSize);
        disp(['Split ' num2str(ff) ' Starts!']); tic;
        if ~exist(splitHDatasetFileName,'file')
            h5create(splitHDatasetFileName,'/data',[patchSize splitFilePatchNum],'Datatype','single');
            h5create(splitHDatasetFileName,'/label',splitFilePatchNum,'Datatype','single');
            h5create(splitHDatasetFileName,'/sample_weight',splitFilePatchNum,'Datatype','single');
        end;
        tic;
        splitData = h5read(trainHDatasetFileName,'/data', [1 1 1 beginPatchId], [patchSize curSplitPatchNum]); toc;
        tic;
        splitLabel = h5read(trainHDatasetFileName,'/label', beginPatchId, curSplitPatchNum);
        splitWeight = h5read(trainHDatasetFileName,'/sample_weight', beginPatchId, curSplitPatchNum);toc;
        %% Write
        disp(['Writing ' splitHDatasetFileName]); tic;
        h5write(splitHDatasetFileName,'/data',single(splitData), [1 1 1 1], [patchSize curSplitPatchNum]);
        h5write(splitHDatasetFileName,'/label',splitLabel, 1, curSplitPatchNum);
        h5write(splitHDatasetFileName,'/sample_weight',splitWeight, 1, curSplitPatchNum); toc;
        disp(['Split ' num2str(ff) ' Done!']); toc;
    end; % ff
end; % ww