function [topMatch_ovlpgt_scores] = create_HDF5_test_Dataset_RGB_FGMask(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
recreateDataset = 1;
shuffleBatch = 1;
maskBG = 1; % 6 channels
orgBatChannels = 4;
if maskBG
    newBatChannels = 6; % {[R G B].*FGMask && {[R G B].*BGMask}
else
    newBatChannels = orgBatChannels; % {[R G B].*FGMask + Shape}
end;
%%
fs = filesep;
dataPath = retGregDataPath();
netDataPath = [dataPath 'singleNet/']; %'/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
targetClassNameList = {'aeroplane'}; % {'aeroplane', 'person'}
for tt = 1 : length(targetClassNameList)
    targetClassName = targetClassNameList{tt} ; % 'aeroplane';
    netClsDataPath = [netDataPath targetClassName '/'];
    sampleDataPath = [netClsDataPath 'samples/'];
    orgHDFPath = [netClsDataPath 'hdf5/test_batch_35x35/']; %['/scratch/stephenchen/shapes/singleNet/aeroplane/hdf5/test_batch_35x35/']; % 
    if maskBG
        hdfMaskPath = [netClsDataPath 'hdf5_FGBGMask/'];
    else
        hdfMaskPath = [netClsDataPath 'hdf5_BGMask/'];
    end;
    if ~isdir(hdfMaskPath) mkdir(hdfMaskPath); end;
    if ~exist('winSizeList','var')
        winSizeList = [35];%[7 13 19];
    end;
    %%%%
    subwinSize = 35;
    %posPatchNum = totalPosPatchNum; % posInfo.Datasets(1).Dataspace.Size(4);
    %negPatchNum = totalNegPatchNum; % negInfo.Datasets(1).Dataspace.Size(4);
    orgPatchSize = [subwinSize subwinSize orgBatChannels]; %posInfo.Datasets(1).Dataspace.Size(1:3);
    newPatchSize = [subwinSize subwinSize newBatChannels];
    batchSize = 100; % predefined
    batPosNum = 40;
    batNegNum = 60;

    subwinSize = orgPatchSize(1);
    testHDFFolder = sprintf('%stest_batch_%dx%d/',hdfMaskPath,subwinSize,subwinSize); if ~isdir(testHDFFolder) mkdir(testHDFFolder); end;
    testHDFFolder = sprintf('%stest_batch_%dx%d/',hdfMaskPath,subwinSize,subwinSize); if ~isdir(testHDFFolder) mkdir(testHDFFolder); end;
    %%
    posLabel = 1;
    negLabel = 0;
    posSampleWeight = single(1);
    negSampleWeight = (batPosNum/batNegNum); % (posPatchNum/negPatchNum);
    orgHDFList = dir([orgHDFPath '*.h5']);
    dataSubsetNum = length(orgHDFList);
    %%% repetitive
    %%
    for dd = 1 : dataSubsetNum
        orgDatasetFileName = sprintf('%s%s',orgHDFPath, orgHDFList(dd).name);
        orgSubsetInfo = h5info(orgDatasetFileName);
        batchNumPerSubset = orgSubsetInfo.Datasets(1).Dataspace.Size(4)/batchSize; % you can predefine too
        patchNumPerSubset = batchNumPerSubset * batchSize;        
        %% Create HDF5 dataset
        curPosPatchIdx = (dd-1) * batchNumPerSubset * batPosNum + 1;
        curNegPatchIdx = (dd-1) * batchNumPerSubset * batNegNum + 1;
        testDatasetFileName = sprintf('%stestHDF_%d_%dx%d.h5',testHDFFolder, dd, subwinSize, subwinSize);
        disp(['Creating ' testDatasetFileName]);
        if ~exist(testDatasetFileName,'file') || recreateDataset
            if recreateDataset
                delete(testDatasetFileName); end;
            % hdfDataNum = totalPatchNum; % Inf
            h5create(testDatasetFileName,'/data',[subwinSize subwinSize newBatChannels patchNumPerSubset],'Datatype','single');%,'ChunkSize',[subwinSize subwinSize 1 1]);
            h5create(testDatasetFileName,'/label',patchNumPerSubset,'Datatype','single');%,'ChunkSize',[1]);
            h5create(testDatasetFileName,'/sample_weight',patchNumPerSubset,'Datatype','single');%,'ChunkSize',[1]);
        end;
        
        
        for bid = 1 : batchNumPerSubset
            if mod(bid,50) == 1
                tic;
            end;
            %% Mask Foregrounds
            idxInDB = (bid - 1) * batchSize + 1;
            orgBatData = h5read(orgDatasetFileName,'/data', [1 1 1 idxInDB],[orgPatchSize batchSize]);
            orgBatLabels = h5read(orgDatasetFileName,'/label', idxInDB, batchSize);
            orgBatWeights = h5read(orgDatasetFileName, '/sample_weight', idxInDB, batchSize);
            newBatData = zeros([newPatchSize batchSize]);
            for pp = 1 : batchSize
                objMask = orgBatData(:,:,4,pp);
                mask3d = repmat(objMask,[1 1 3]);
                newBatData(:,:,1:3,pp) = orgBatData(:,:,1:3,pp) .* mask3d;
                if maskBG
                    mask3d = repmat(double(~objMask),[1 1 3]);
                    newBatData(:,:,4:6,pp) = orgBatData(:,:,1:3,pp) .* mask3d;
                end;
            end;
            %orgBatData = permute(orgBatData, [4 3 2 1]); % have shape [num, channel, width, height]
            %%
            h5write(testDatasetFileName,'/data',single(newBatData), [1 1 1 idxInDB], size(newBatData)); %[batchSize newPatchSize]);
            h5write(testDatasetFileName,'/label',orgBatLabels, idxInDB, batchSize);
            h5write(testDatasetFileName,'/sample_weight',orgBatWeights, idxInDB, batchSize);           
            if mod(bid,50) == 0
                bt = toc;
                disp(['Batch ' num2str(bid) ' Done, elapsed time: ' num2str(bt) 's']);
            end;
        end;
    end; % dd
end; % tt
