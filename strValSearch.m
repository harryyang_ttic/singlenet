function [vals] = strValSearch(strCell, strPatt)
percPos = strfind(strPatt,'%');
strPrefix = strPatt(1:(percPos(1)-1));

ids = strSearch(strCell,strPrefix);
vals = [];
for ss = 1 : length(ids)
    patStartPos = strfind(strCell{ids(ss)}, strPrefix);
    val = sscanf(strCell{ids(ss)}(patStartPos:end),strPatt);
    vals = [vals; val];
end;