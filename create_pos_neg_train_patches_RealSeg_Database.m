function [topMatch_ovlpgt_scores] = create_pos_neg_train_patches_RealSeg_Database(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
countTrainNum = 0;
posOvlpThrsh = 0.8;
negOvlpThrsh = 0.5;
if ~exist('winSizeList','var')
    %     winSizeList = [3 5 7 9 11 13 15 17 19 21 23 25 27];
    winSizeList = [35];%[7 13 19];
    % winSizeList = [3 7 11 15 19 23 27];
end;
%% Padding
dx = 0;%ceil(max(winSizeList) /4);
dy = dx;
padr = ceil(max(winSizeList) /2)+dx;
%%
%%
fs = filesep;
realSegPath = '../../data/segment_results/voc12berkeley/';
codePath = '../codes/'; addpath(codePath);
[vocPath, imgPath, gtPath, imgSetPath] = VOCDataPaths();
saveTrainPrefix = 'trainHDF';
minMaskPixThrsh = 50; % min size of mask should be 100

%%
%dsInfo = mapDataSets('voc12','fg-all','aeroplane');
addpath('../libs/shape_context/');
addpath('../VOCcode');

VOCinit;
%[extTrainFileList,gt]=textread(sprintf(VOCopts.imgsetpath,['../Segmentation/train']),'%s %d');
%[extTrainFullList, extTrainFileList, extDatasetNames, sbdPath] = getExpandTrainList_VOC();
%dsInfo=mapDataSets('voc12','all-val','class')

%sbdInstPath = [sbdPath 'inst/']; sbdClsPath = [sbdPath 'cls/'];
%extTrainImgNum = length(extTrainFileList);
%[valList,gt]=textread(sprintf(VOCopts.imgsetpath,['../Segmentation/val']),'%s %d');
%valImgNum = length(valList);
%classNameList = VOCopts.classes;
targetClassNameList = {'bicycle','person'};
for tt = 1 : length(targetClassNameList)
    targetClassName = targetClassNameList{tt};
    netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
    % netDataPath = [netDataPath targetClassName '/']; if ~isdir(netDataPath) mkdir(netDataPath); end;
    sampleDataPath = [netDataPath 'samples/'];
    % posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
    posNegPatchPath='../';
    if ~exist('nsampPos','var')
        nsampPos = 40; end;
    % targetClassLabel = find(strcmp(classNameList,targetClassName));
    close all; 
    %%%Define flags and parameters:
    %% Parse Vararin
    for pair = reshape(varargin,2,[])
        tmp = pair{2};
        eval([pair{1} ' = tmp']);
        clear tmp;
    end; %pair
    %% get windows around sample points
    
    for ww = 1 : length(winSizeList)
        subwinSize = winSizeList(ww); % sxs window around sample points
        disp(['---- Window Size ' num2str(subwinSize)]);
        winR = floor(subwinSize/2);
        %% Create Pos HDF5 dataset
        trainPosDatasetFileName = sprintf('%strainPosPatches_RealPos_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
        if ~exist(trainPosDatasetFileName,'file')
            h5create(trainPosDatasetFileName,'/data',[subwinSize subwinSize 4 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 4 1]);
            h5create(trainPosDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
            h5create(trainPosDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
        end;
        %%%
        trainNegDatasetFileName = sprintf('%strainNegPatches_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
        if ~exist(trainNegDatasetFileName,'file')
            h5create(trainNegDatasetFileName,'/data',[subwinSize subwinSize 4 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 4 1]);
            h5create(trainNegDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
            h5create(trainNegDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
        end;
        %%
        posLabel = single(1);
        negLabel = single(0);
        curPosPatchIdx = 0;
        curNegPatchIdx = 0;
        predClsRealSegNum = 0; % number of hypothesized segmentation predicted with the target class
        %% Posative data
        visited = 0;
      %  load([sampleDataPath sprintf('SingleNetTrainPos_%s_20.mat',targetClassName)],'maskW','maskH');
      %  meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
        posCountFileName = sprintf('%s%s_%s_%dx%d.mat', sampleDataPath, 'posTrainCount_RealSeg',targetClassName, subwinSize, subwinSize);
     %   negCountFileName = [sampleDataPath 'negTrainCount.mat'];
     %   if ~exist(posCountFileName,'file') || countTrainNum || ~exist(negCountFileName,'file')
     %       totalPosPatchNum = 0;
     %       totalNegPatchNum = 0;
     %       recountFlags = [1 0]; % recount training patches from real images
     %   else
     %       load(posCountFileName,'totalPosPatchNum');
     %       load(negCountFileName,'totalNegPatchNum');
     %       recountFlags = [0]; % not counting
     %   end;
     %   refMaskSize = [meanMH meanMW];
        for countPatchNum = recountFlags
            if ~countPatchNum
                posRandIdx = randperm(totalPosPatchNum);
                negRandIdx = randperm(totalNegPatchNum);
                load(posCountFileName,'totalPosPatchNum');
                load(negCountFileName,'totalNegPatchNum');
                posSampleWeight = single(1);
                negSampleWeight = single(totalPosPatchNum / totalNegPatchNum);
            end;
            for mm = 1 : extTrainImgNum
                imgFullName = extTrainFullList{mm};
                [imgDir imgName imgFormat] = fileparts(imgFullName);
                %% debug
                            debugFlag = 1;
                            if debugFlag
                                debugOrgImgName = '2011_003238';
                                debugRealSegName = '2011_003238_S0012';
                                if ~strcmp(imgName,debugOrgImgName) && visited == 0
                                    continue;
                                elseif strcmp(imgName,debugOrgImgName)
                                    visited = 1;
                                    %curPatchIdx = 1087735;
                                end;
                            end;
                %%
                orgImage = im2double(imread(imgFullName));
                imgH = size(orgImage,1); imgW = size(orgImage,2);
                switch extDatasetNames{mm}
                    case 'sbd'
                        load([sbdInstPath imgName '.mat'],'GTinst');
                        load([sbdClsPath imgName '.mat'],'GTcls');
                        Sclass = GTcls.Segmentation;
                        Sobj = GTinst.Segmentation;
                        objInstIdx = unique(Sobj);
                        objInstIdx = setdiff(objInstIdx, 0);%
                        objInstIdx = setdiff(objInstIdx, 255);
                        imgClassIdxList = GTinst.Categories;
                    case 'voc12'
                        rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
                        imgClassList = {rec.objects.class};
                        objInstIdx = find(strcmp(imgClassList,targetClassName));
                        %if length(objInstIdx)
                        [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                        [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
                end;
                imgRealSegFolder = [realSegPath imgName fs];
                imgRealSegList = dir([imgRealSegFolder '*.png']);
                targetClassGTMask = double(Sclass == targetClassLabel);
                for rr = 1 : length(imgRealSegList)
                    tic;
                    %%
                                    if debugFlag
                                        if ~strcmp(imgName,debugRealSegName)
                                            continue;
                                        elseif strcmp(imgName,debugOrgImgName)
                                            curPosPatchIdx = 888243;
                                        end;
                                    end;
                    %%
                    [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
                    realClsIdx = setdiff(unique(realSegImg),[0 255]);
                    writeFlag = 0;
                    if isempty(find(realClsIdx==targetClassLabel))
                        %disp(['Predicted Segmentation has no ' targetClassName ', continue!']);
                        continue;
                    else
                        disp(['Predicted Segmentation has ' targetClassName '!']);
                        writeFlag = 1;
                    end;
                    fprintf('%s Starts\n',[imgRealSegList(rr).name]);
                    predClsRealSegNum = predClsRealSegNum + 1;
                    realInstMask = double(realSegImg == targetClassLabel);%realClsIdx(rii));
                    [r c v] = find(realInstMask);
                    if length(find(realInstMask)) < minMaskPixThrsh
                        disp('Mask Too Small, Dump!'); continue; end;
                    bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
                    orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = imresize(realInstMask,refMaskSize,'bilinear');
                    orgInstImg = imresize(orgInstImg,refMaskSize);
                    realInstGTMask = imresize(realInstGTMask,refMaskSize);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = padarray(realInstMask,[padr padr]);
                    orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
                    realInstGTMask = padarray(realInstGTMask,[padr padr]);
                    %%%%%%%%%%%%%%%%%%%%%
                    %% sample points for patches
                    [xre,yre,tre]=bdry_extract_3(realInstMask);
                    nsamp2=length(xre);
                    if nsamp2>=nsampPos
                        [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampPos);
                    end
                    xre = round(xre); yre = round(yre);
                    %% Sample posative patches along false positive boundaries
                    for ii = 1 : nsampPos
                        %$% No Translation
                        for tx = 0 %-dx : dx: dx % translation x
                            for ty = 0 %-dy : dy: dy
                                trainPatchMask = realInstMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
                                trainPatchGTMask = realInstGTMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
                                % ovlpGT = length(find(trainPatchMask .* trainPatchGTMask)) / length( find(trainPatchMask + trainPatchGTMask));
                                ovlpGT = fbIntUni(trainPatchMask, trainPatchGTMask);
                                if ovlpGT >= posOvlpThrsh % 0.8
                                    patchGTLabel = 1;
                                elseif ovlpGT <= negOvlpThrsh % 0.3
                                    patchGTLabel = 0;
                                else
                                    continue;
                                end;
                                if countPatchNum
                                    if patchGTLabel == 1
                                        totalPosPatchNum = totalPosPatchNum + 1; % one more positive patch from real segmentation
                                        if mod(totalPosPatchNum,10)==0
                                            disp(['Positive ' num2str(totalPosPatchNum) ' Data']);end;
                                    else
                                        totalNegPatchNum = totalNegPatchNum + 1;
                                        if mod(totalNegPatchNum, 100)==0
                                            disp(['Negative ' num2str(totalNegPatchNum) ' Data']); end;
                                    end;
                                    continue; % only count, not write to dataset
                                end;
                                trainPatchImg = orgInstImg( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR), : );
                                trainPatchImg(:,:,end+1) = trainPatchMask;
                                %% Write to HDF5
                                if patchGTLabel == 1
                                    curPosPatchIdx = curPosPatchIdx + 1;
                                    idxPosInH5 = find(posRandIdx == curPosPatchIdx);
                                    h5write(trainPosDatasetFileName,'/data',single(trainPatchImg), [1 1 1 idxPosInH5], [size(trainPatchImg) 1]);
                                    h5write(trainPosDatasetFileName,'/label',posLabel, idxPosInH5, 1);
                                    h5write(trainPosDatasetFileName,'/sample_weight',posSampleWeight, idxPosInH5, 1);
                                elseif patchGTLabel == 0
                                    curNegPatchIdx = curNegPatchIdx + 1;
                                    idxNegInH5 = find(negRandIdx == curNegPatchIdx);
                                    h5write(trainNegDatasetFileName,'/data',single(trainPatchImg), [1 1 1 idxNegInH5], [size(trainPatchImg) 1]);
                                    h5write(trainNegDatasetFileName,'/label',negLabel, idxNegInH5, 1);
                                    h5write(trainNegDatasetFileName,'/sample_weight',negSampleWeight, idxNegInH5, 1);
                                end;
                            end; % ty
                        end; % tx
                    end;
                    if ~countPatchNum && writeFlag
                        disp(['Positive ' num2str(curPosPatchIdx) ' Data Written']);
                        disp(['Negative ' num2str(curNegPatchIdx) ' Data Written']);
                    end;
                end; % rii
                rrTime = toc;
                disp([imgRealSegList(rr).name ' takes ' num2str(rrTime) 's']);
                %             end;  % rr
            end; % mm
            if  countPatchNum 
                save(posCountFileName,'totalPosPatchNum','predClsRealSegNum');
                save(negCountFileName,'totalNegPatchNum','predClsRealSegNum');
            end;
        end; % for  countPatchNum
    end; % ww
end; % tt: targetClassNameList = {'bicycle','person'};

return;

function [ovlp] = fbIntUni(img1, img2)
% def: fore/background intersect/union: = (f^f'/fUf' + b^b'/bUb')/2
inter = img1.*img2;
uni = double(img1+img2);
fgivu = length(find(inter))/length(find(img1+img2));
bgivu = length(find(~uni)) / length(find(~inter));
ovlp = ( fgivu + bgivu ) /2;
debugFlag = 0;
if debugFlag && fgivu
    subaxisParam = {'Spacing', 0.05, 'Padding', 0, 'Margin', 0.05};
    %%
    subaxis(2,3,1, subaxisParam{:});
    axis tight;axis off;
    imshow(img1);
    hold on;title(['A (fg int/uni = ' num2str(fgivu) ')']); hold off
    %%
    subaxis(2,3,4, subaxisParam{:});
    axis tight;axis off;
    imshow(img2);
    hold on;title(['B (bg int/uni = ' num2str(bgivu) ')']); hold off
    %%
    subaxis(2,3,2, subaxisParam{:});
    axis tight;axis off;
    imshow(inter);
    hold on;title('A\capB'); hold off
    %% Hypothesized masks
    subaxis(2,3,3, subaxisParam{:});
    axis tight;axis off;
    imshow(0.3*img1+0.7*img2);
    hold on;title('A\cupB'); hold off
    %% Groundtruth
    subaxis(2,3,5, subaxisParam{:});
    axis tight;axis off;
    imshow(~uni);
    hold on;title('~A\cap~B'); hold off
    %% Groundtruth
    subaxis(2,3,6, subaxisParam{:});
    axis tight;axis off;
    imshow(~inter);
    hold on;title('~A\cup~B'); hold off
end;
%%