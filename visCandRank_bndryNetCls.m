function [topPredId, boundaryGain, maskGain] = visCandRank_bndryNetCls(gtInstMaskList, windowSize, instMaskList, ...
     pointList, predScoreList, patchOvlpGTList, maskOvlpGTScores, varargin)
%(orgImage, instMask, gtMask,...
%    batchPatchMasks, points, predScores, ovlpGT, titleStrs,varargin)
%% Parse Input Arguments
for pair = reshape(varargin{:},2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp;']);
    clear tmp;
end; %pair
colorBarFlag = 0;
close all;
%%
if colorBarFlag
    % subaxisParam = {'Spacing', 0.01, 'Padding', 0, 'Margin',0.1,'MarginRight', 0.15 };
else
    subaxisParam = {'Spacing', 0.1, 'Padding', 0, 'Margin',0.1};
end;
if exist('img1','var') && exist('img2','var')
    plotrow = 2;
else
    plotrow = 1;
end;
wr = windowSize/2;
%%
orgTopImgId = 1;
realSegNum = length(instMaskList);
if realSegNum == 0 return; end;
topkNum = 10;
topkNum = min(topkNum, realSegNum);
if topkNum < 10
    plotRows = floor(sqrt(topkNum)); %5;
    plotCols = ceil(topkNum / plotRows);
else
    plotCols = round(sqrt(topkNum)); %5;
    plotRows = ceil(topkNum / plotCols);
end;
%% Compute the Ranking
segBoundaryScores = [];
gtBoundaryScores = [];
for imgId = 1 : realSegNum
    predScores = predScoreList{imgId};
    ovlpGT = patchOvlpGTList{imgId};
    instMask = instMaskList{imgId};
    segBoundaryScores(end+1) = sum(predScores);
    gtBoundaryScores(end+1) = sum(ovlpGT);
end; 
%% Compute Rank and Gain
[predVal predictRank] = sort(segBoundaryScores,'descend');
[gtBdryVal gtBdryRank] = sort(gtBoundaryScores,'descend');
[gtMaskVal gtMaskRank] = sort(maskOvlpGTScores,'descend');
topPredId = predictRank(1);
oracleId = gtMaskRank(1);
predBdryRank = find(gtBdryRank == topPredId);
predMaskRank = find(gtMaskRank == topPredId);
orgBdryRank = find(gtBdryRank == 1);
orgMaskRank = find(gtMaskRank == 1);
boundaryGain = (gtBdryVal(predBdryRank) - gtBdryVal(orgBdryRank)) / (gtBdryVal(orgBdryRank)+eps);
maskGain = (gtMaskVal(predMaskRank) - gtMaskVal(orgMaskRank))/ (gtMaskVal(orgMaskRank) + eps);
%%
colormap default;
curmap = colormap;
curmap = [0 0 0; curmap];
%curmap = [curmap; 1 1 1];
showRank = predictRank(1:topkNum);
%if ~ismember(orgTopImgId, showRank)
    showRank(end+1) = orgTopImgId;
%end;
%if ~ismember(oracleId, showRank)
    showRank(end+1) = oracleId; 
%end;
for pp = 1 : length(showRank) % length(predictRank)
    tic;
    imgId = showRank(pp);
    segPredBrdyRank = find(predictRank == imgId);
    segGTBdryRank = find(gtBdryRank == imgId);
    segMaskRank = find(gtMaskRank == imgId);
    %%
    orgImage = gtInstMaskList{imgId};
    instMask = instMaskList{imgId};
    points = pointList{imgId};
    predScores = predScoreList{imgId};
    ovlpGT = patchOvlpGTList{imgId};    
    nsamps = size(points,1);
    %%
    
    line1 = sprintf('OrgId = %d, Mask(Rank,Val) = (%d, %.2f)', ...
        imgId, segMaskRank, gtMaskVal(segMaskRank));
    line2 = sprintf('GTBdry(Rank,Val) = (%d, %.2f)',segGTBdryRank, gtBdryVal(segGTBdryRank)/nsamps); 
    line3 = sprintf('BdryNet(Rank,Val) = (%d, %.2f)',  segPredBrdyRank, predVal(segPredBrdyRank)/nsamps);
    titleStrs = {line1,line2,line3};
    %% calculate the average ovlp
    avgOvlp = avgNearScores(points, wr, ovlpGT);
    %%
    xre = points(:,1); yre = points(:,2);
    posIdx = find(avgOvlp >= 0.5);
    negIdx = find(avgOvlp < 0.5);
    ovlpGTLabels(posIdx) = 1;
    ovlpGTLabels(negIdx) = 0;
    clsLabels = (predScores > 0.5)';
    truePosId = find(clsLabels == 1 & ovlpGTLabels == 1);
    trueNegId = find(clsLabels == 0 & ovlpGTLabels == 0);
    falsePosId = find(clsLabels == 1 & ovlpGTLabels == 0);
    falseNegId = find(clsLabels == 0 & ovlpGTLabels == 1);
    evalLabel = zeros(size(clsLabels));
    evalLabel(truePosId) = 1; evalLabel(trueNegId) = 0;
    evalLabel(falsePosId) = 2; evalLabel(falseNegId) = 3;
    % eval(posIdx) = (clsLabels(posIdx) == ovlpGTLabels(posIdx));
    % eval(negIdx) = (clsLabels(negIdx) == ovlpGTLabels(negIdx));
    %%
    %% Showing annotation
    %imshow(instMask);
    h4 = subaxis(plotRows,plotCols,pp,subaxisParam{:});
    axis tight
    axis off
    wr = floor(windowSize/2);
    hold on;
    imshow(color_seg_rgb(instMask, orgImage));
    %%%%%
    %     if colorBarFlag
    %         originalSize4 = get(gca, 'Position');
    %         hCbar4 = colorbar;
    %         set(h4, 'Position', originalSize4);
    %     end;
    %%%%
    boundaryPtsBox(xre, yre, windowSize, evalLabel);%,1 );

    colormap(curmap);
    %%%%
    %     if colorBarFlag
    %         set(hCbar2,'YTick',[0 0.2 0.8 1]);
    %         set(hCbar2,'YTickLabel',{'TrueBG','FalsePos','FalseNeg','TruePos'});
    %         %%%%
    %         set(hCbar4,'YTick',[0:0.1:1]);
    %     end;
    %%%%%
    a = 30;
    nsamp = length(xre);
    cmIds = round(linspace(1, size(curmap,1), nsamp));
    [val sortedInOrg] = sort(avgOvlp,'ascend');
    [val rank] = sort(sortedInOrg,'ascend');
    hold on;
    scatter(xre,yre,a,curmap(cmIds(rank),:),'filled'); hold on;
    %%%%%
    title(titleStrs);
    hold off
    toc;
end; % pp
set(gcf,'outerposition', get(0,'screensize'));
%% save image
if exist('saveImgName','var')
    %set(gcf,'outerposition', get(0,'screensize'));
    saveas(gcf, saveImgName);
end;







%%
function [avgScores] = avgNearScores(pts, radius, orgScores)
dists = pdist2(pts, pts);
avgScores = zeros(size(orgScores));
for pp = 1 : size(pts,1)
    ptDist = dists(pp,:);
    nnIds = find(ptDist <= radius);
    avgScores(pp) = mean(orgScores(nnIds));
end;



%%
%saveas(gcf, [saveVisFolder imgName '.png']);
function [] = boundaryPtsBox(xre, yre, ws, colorMap,legendFlag)
if ~exist('legendFlag','var') legendFlag = 0; end;
nsamps = length(xre); 
wr = ws/2;
handles = cell(1,4);
visited = zeros(1,4);
for ii = 1 : nsamps
    px = xre(ii); py = yre(ii);
    switch colorMap(ii)
        case 1 % true positive
            boxColor = 'g';
        case 0 % true negative
            boxColor = 'r';
        case 2 % false positive
            boxColor = 'y';
        case 3 % false negative
            boxColor = 'b';
        otherwise
            boxColor = 'm';
    end;
    hold on;
    h1 = rectangle('Position',[px-wr py-wr ws ws],'EdgeColor',boxColor); 
    if legendFlag && visited(colorMap(ii)+1) == 0
        p1=plot(nan,nan,'s','markeredgecolor',get(h1,'edgecolor'),...
            'markerfacecolor',get(h1,'facecolor'));
        handles{colorMap(ii)+1} = p1;
        visited(colorMap(ii)+1) = 1;
    end;
    % plot(px,py,'r*');
    axis image;
end; %ii
if legendFlag
    legend(cell2mat(handles),{'true neg','true pos','false pos','false neg'},'Location','southeast')
end;

