function [topMatch_ovlpgt_scores] = create_HDF5_train_Dataset(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%

recreateDataset = 0;
shuffleBatch = 1;
%%
fs = filesep;
dataPath = retGregDataPath();
netDataPath = [dataPath 'singleNet/']; %'/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
targetClassNameList = {'person'}; % {'aeroplane', 'person'}
for tt = 1 : length(targetClassNameList)
    targetClassName = targetClassNameList{tt} ; % 'aeroplane';
    netClsDataPath = [netDataPath targetClassName '/'];
    sampleDataPath = [netClsDataPath 'samples/'];
    hdfPath = [netClsDataPath 'hdf5/']; if ~isdir(hdfPath) mkdir(hdfPath); end;
    if ~exist('winSizeList','var')
        winSizeList = [35];%[7 13 19];
    end;
    posNegPatchPath = [netClsDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
    posTrainHFilName = [posNegPatchPath 'trainPosPatches_RealPos_35x35.h5'];% 'trainPosPatches_35x35.h5'];
    negTrainHFilName = [posNegPatchPath 'trainNegPatches_35x35.h5'];
    %posInfo = h5info(posTrainHFilName);
    %negInfo = h5info(negTrainHFilName);
    %%%%
    subwinSize = 35;
    posCountFileName = sprintf('%s%s_%dx%d.mat', sampleDataPath, 'posTrainCount_RealSeg', subwinSize, subwinSize);
    negCountFileName = [sampleDataPath 'negTrainCount.mat'];
    load(posCountFileName,'totalPosPatchNum');
    load(negCountFileName,'totalNegPatchNum');
    posPatchNum = totalPosPatchNum; % posInfo.Datasets(1).Dataspace.Size(4);
    negPatchNum = totalNegPatchNum; % negInfo.Datasets(1).Dataspace.Size(4);
    patchSize = [subwinSize subwinSize 4]; %posInfo.Datasets(1).Dataspace.Size(1:3);
    batchSize = 100; % predefined
    batPosNum = 40;
    batNegNum = 60;
    batchNum = ceil(negPatchNum/batNegNum); % you can predefine too
    dataSubsetNum = 20;
    batchNumPerSubset = floor(batchNum/dataSubsetNum);
    patchNumPerSubset = batchNumPerSubset * batchSize;
    subwinSize = patchSize(1);
    trainHDFFolder = sprintf('%strain_batch_%dx%d/',hdfPath,subwinSize,subwinSize); if ~isdir(trainHDFFolder) mkdir(trainHDFFolder); end;
    testHDFFolder = sprintf('%stest_batch_%dx%d/',hdfPath,subwinSize,subwinSize); if ~isdir(testHDFFolder) mkdir(testHDFFolder); end;
    %%
    posLabel = 1;
    negLabel = 0;
    posSampleWeight = single(1);
    negSampleWeight = (batPosNum/batNegNum); % (posPatchNum/negPatchNum);
    %%
    curPosPatchIdx = 1;
    curNegPatchIdx = 1;
    %%% repetitive
    posLabels = posLabel * ones(batPosNum,1);
    posWeights = posSampleWeight * ones(batPosNum,1);
    negLabels = negLabel * ones(batNegNum,1);
    negWeights = negSampleWeight * ones(batNegNum,1);
    %%
    for dd = 1 : dataSubsetNum
        %% Create HDF5 dataset
        curPosPatchIdx = (dd-1) * batchNumPerSubset * batPosNum + 1;
        curNegPatchIdx = (dd-1) * batchNumPerSubset * batNegNum + 1;
        trainDatasetFileName = sprintf('%strainHDF_%d_%dx%d.h5',trainHDFFolder, dd, subwinSize, subwinSize);
        disp(['Creating ' trainDatasetFileName]);
        if ~exist(trainDatasetFileName,'file') || recreateDataset
            if recreateDataset
                delete(trainDatasetFileName); end;
            totalPatchNum = batchSize * batchNum;
            % hdfDataNum = totalPatchNum; % Inf
            h5create(trainDatasetFileName,'/data',[subwinSize subwinSize 4 patchNumPerSubset],'Datatype','single');%,'ChunkSize',[subwinSize subwinSize 1 1]);
            h5create(trainDatasetFileName,'/label',patchNumPerSubset,'Datatype','single');%,'ChunkSize',[1]);
            h5create(trainDatasetFileName,'/sample_weight',patchNumPerSubset,'Datatype','single');%,'ChunkSize',[1]);
        end;
        
        
        for bid = 1 : batchNumPerSubset
            if mod(bid,10) == 1
                tic;
            end;
            posBatData = h5read(posTrainHFilName,'/data', [1 1 1 curPosPatchIdx],[patchSize batPosNum]);
            
            
            negBatData = h5read(negTrainHFilName,'/data', [1 1 1 curNegPatchIdx],[patchSize batNegNum]);
            
            
            batchLabels = single([posLabels; negLabels]);
            batchWeights = single([posWeights; negWeights]);
            batchData = zeros([patchSize batchSize]);
            batchData(:,:,:,1:batPosNum) = posBatData;
            batchData(:,:,:,(batPosNum+1):batchSize) = negBatData;
            batchData = single(batchData);
            
            if shuffleBatch
                batRandIdx = randperm(batchSize);
                batchData = batchData(:,:,:,batRandIdx);
                batchLabels = batchLabels(batRandIdx);
                batchWeights = batchWeights(batRandIdx);
            end;
            %%% Write to HDF5
            idxInDB = (bid - 1) * batchSize + 1;
            h5write(trainDatasetFileName,'/data',single(batchData), [1 1 1 idxInDB], [patchSize batchSize]);
            h5write(trainDatasetFileName,'/label',batchLabels, idxInDB, batchSize);
            h5write(trainDatasetFileName,'/sample_weight',batchWeights, idxInDB, batchSize);
            %%%
            curPosPatchIdx = mod(curPosPatchIdx + batPosNum,posPatchNum);
            if curPosPatchIdx + batPosNum > posPatchNum
                curPosPatchIdx = posPatchNum - batPosNum +1;
            end;
            curNegPatchIdx = mod(curNegPatchIdx + batNegNum,negPatchNum);
            if curNegPatchIdx + batNegNum > negPatchNum
                curNegPatchIdx = negPatchNum - batNegNum +1;
            end;
            if mod(bid,10) == 0
                bt = toc;
                disp(['Batch ' num2str(bid) ' Done, elapsed time: ' num2str(bt) 's']);
            end;
        end;
    end; % dd
end; % tt
