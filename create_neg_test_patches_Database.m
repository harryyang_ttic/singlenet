function [topMatch_ovlpgt_scores] = create_neg_test_patches_Database(nsampPos, winSizeList,varargin)
% load in the digit database (only needs to be done once per session)
%%
countNegSample = 1;
regetNegTestData = 1;
%%
fs = filesep;
netDataPath = '/scratch/stephenchen/shapes/singleNet/'; %'/share/project/shapes/singleNet/'; % [pwd fs];
sampleDataPath = '/share/project/shapes/singleNet/samples/';
targetClassName = 'aeroplane';
if ~exist('winSizeList','var')
    %     winSizeList = [3 5 7 9 11 13 15 17 19 21 23 25 27];
    winSizeList = [35];%[7 13 19];
    % winSizeList = [3 7 11 15 19 23 27];
end;
if ~exist('nsampPos','var')
    nsampPos = 20; end;
savePosTrainPreFileName = ['SingleNetTrainPos_' targetClassName '_' num2str(nsampPos) '.mat'];%['VOCShapeTestSet_' targetClassName '_' num2str(nsampPos) '.mat'];
% saveNegTestFileName = ['SingleNetTestNeg_' targetClassName '_' num2str(nsampPos) '.mat'];
realSegPath = '/share/project/shapes/data/segment_results/voc12berkeley/';
codePath = '/share/project/shapes/codes/'; addpath(codePath);
[vocPath, imgPath, gtPath, imgSetPath] = VOCDataPaths();
posNegPatchPath = [netDataPath 'pos_neg_patches_DB/']; if ~isdir(posNegPatchPath) mkdir(posNegPatchPath); end;
saveTestPrefix = 'testHDF';
minMaskPixThrsh = 50; % min size of mask should be 100

%%
% dsInfo = mapDataSets('voc12','fg-all',targetClassName);
addpath('/share/project/shapes/libs/shape_context/');
addpath('/share/project/shapes/vocdevkit/VOCcode');

VOCinit;
%% Image sets
dsInfo = mapDataSets('voc12','val','inst');
valList = dsInfo.imnames;
testImgNum = length(valList);
valImgNum = length(valList);

classNameList = VOCopts.classes;
targetClassLabel = find(strcmp(classNameList,targetClassName));
close all;


saveScoreFolder = '../data/cache_voc2012/';
%load([netDataPath saveNegTestFileName],'negData','negInstMasks','negInstImages','negInstGTMasks','maskW','maskH');

if ~exist('beginNum','var')
    beginNum = 1; end;
if ~exist('endNum','var')
    endNum = 1; end;
if ~exist('distortNum','var')
    distortNum = 10; % generate distorted query image
end;

%%%Define flags and parameters:
%%
winSizeNum = length(winSizeList);
tau = 0.7
%%
% testPosInstNum = length(posInstMasks);
queryNum = endNum - beginNum + 1;% length(testSet);
pixValThsh = 0.5;
r=1; % annealing rate
w=4;
sf=1;

%% Parse Vararin
for pair = reshape(varargin,2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp']);
    clear tmp;
end; %pair
checkDir(saveScoreFolder);
%%  Precompute Testing Images Patches
dx = 0; % No displacement during testing %ceil(max(winSizeList) /2);
dy = 0; % No displacement during testing %dx;
padr = ceil(max(winSizeList) /2)+dx;
%% get windows around sample points

for ww = 1 : length(winSizeList)
    subwinSize = winSizeList(ww); % sxs window around sample points
    disp(['---- Window Size ' num2str(subwinSize)]);
    winR = floor(subwinSize/2);
    %% Create Pos HDF5 dataset
    trainPosDatasetFileName = sprintf('%strainPosPatches_RealPos_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
    if ~exist(trainPosDatasetFileName,'file')
        h5create(trainPosDatasetFileName,'/data',[subwinSize subwinSize 4 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 4 1]);
        h5create(trainPosDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
        h5create(trainPosDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
    end;
    %%%
    trainNegDatasetFileName = sprintf('%strainNegPatches_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
    if ~exist(trainNegDatasetFileName,'file')
        h5create(trainNegDatasetFileName,'/data',[subwinSize subwinSize 4 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 4 1]);
        h5create(trainNegDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
        h5create(trainNegDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
    end;
    %%
    posLabel = single(1);
    negLabel = single(0);
    curPosPatchIdx = 0;
    curNegPatchIdx = 0;
    predClsRealSegNum = 0; % number of hypothesized segmentation predicted with the target class
    %% Posative data
    visited = 0;
    load([sampleDataPath 'SingleNetTrainPos_aeroplane_20.mat'],'maskW','maskH');
    meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
    posCountFileName = sprintf('%s%s_%dx%d.mat', sampleDataPath, 'posTrainCount_FromRealSeg', subwinSize, subwinSize);
    negCountFileName = [sampleDataPath 'negTrainCount.mat'];
    if ~exist(posCountFileName,'file') || countTrainNum || ~exist(negCountFileName,'file')
        totalPosPatchNum = 0;
        totalNegPatchNum = 0;
        recountFlags = [1 0]; % recount training patches from real images
    else
        load(posCountFileName,'totalPosPatchNum');
        load(negCountFileName,'totalNegPatchNum');
        recountFlags = [0]; % not counting
    end;
    refMaskSize = [meanMH meanMW];
    for countPatchNum = recountFlags
        if ~countPatchNum
            posRandIdx = randperm(totalPosPatchNum);
            negRandIdx = randperm(totalNegPatchNum);
            load(posCountFileName,'totalPosPatchNum');
            load(negCountFileName,'totalNegPatchNum');
            posSampleWeight = single(1);
            negSampleWeight = single(totalPosPatchNum / totalNegPatchNum);
        end;
        for mm = 1 : extTrainImgNum
            imgFullName = extTrainFullList{mm};
            [imgDir imgName imgFormat] = fileparts(imgFullName);
            %% debug
            %             debugFlag = 0;
            %             if debugFlag
            %                 debugOrgImgName = '2011_002851';
            %                 debugRealSegName = '2011_002851_S0014';
            %                 if ~strcmp(imgName,debugOrgImgName) && visited == 0
            %                     continue;
            %                 elseif strcmp(imgName,debugOrgImgName)
            %                     visited = 1;
            %                     %curPatchIdx = 1087735;
            %                 end;
            %             end;
            %%
            orgImage = im2double(imread(imgFullName));
            imgH = size(orgImage,1); imgW = size(orgImage,2);
            switch extDatasetNames{mm}
                case 'sbd'
                    load([sbdInstPath imgName '.mat'],'GTinst');
                    load([sbdClsPath imgName '.mat'],'GTcls');
                    Sclass = GTcls.Segmentation;
                    Sobj = GTinst.Segmentation;
                    objInstIdx = unique(Sobj);
                    objInstIdx = setdiff(objInstIdx, 0);%
                    objInstIdx = setdiff(objInstIdx, 255);
                    imgClassIdxList = GTinst.Categories;
                case 'voc12'
                    rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
                    imgClassList = {rec.objects.class};
                    objInstIdx = find(strcmp(imgClassList,targetClassName));
                    %if length(objInstIdx)
                    [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                    [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
            end;
            imgRealSegFolder = [realSegPath imgName fs];
            imgRealSegList = dir([imgRealSegFolder '*.png']);
            targetClassGTMask = double(Sclass == targetClassLabel);
            for rr = 1 : length(imgRealSegList)
                tic;
                %%
                %                 if debugFlag
                %                     if ~strcmp(imgName,debugRealSegName)
                %                         continue;
                %                     elseif strcmp(imgName,debugOrgImgName)
                %                         curPosPatchIdx = 1087735;
                %                     end;
                %                 end;
                %%
                [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
                realClsIdx = setdiff(unique(realSegImg),[0 255]);
                if isempty(find(realClsIdx==targetClassLabel))
                    %disp(['Predicted Segmentation has no ' targetClassName ', continue!']);
                    continue;
                else
                    disp(['Predicted Segmentation has ' targetClassName '!']);
                end;
                fprintf('%s Starts\n',[imgRealSegList(rr).name]);
                predClsRealSegNum = predClsRealSegNum + 1;
                realInstMask = double(realSegImg == targetClassLabel);%realClsIdx(rii));
                [r c v] = find(realInstMask);
                if length(find(realInstMask)) < minMaskPixThrsh
                    disp('Mask Too Small, Dump!'); continue; end;
                bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
                %%%%%%%%%%%%%%%%%%%%%
                realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
                orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
                realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
                %%%%%%%%%%%%%%%%%%%%%
                realInstMask = imresize(realInstMask,refMaskSize,'bilinear');
                orgInstImg = imresize(orgInstImg,refMaskSize);
                realInstGTMask = imresize(realInstGTMask,refMaskSize);
                %%%%%%%%%%%%%%%%%%%%%
                realInstMask = padarray(realInstMask,[padr padr]);
                orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
                realInstGTMask = padarray(realInstGTMask,[padr padr]);
                %%%%%%%%%%%%%%%%%%%%%
                %% sample points for patches
                [xre,yre,tre]=bdry_extract_3(realInstMask);
                nsamp2=length(xre);
                if nsamp2>=nsampPos
                    [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampPos);
                end
                xre = round(xre); yre = round(yre);
                %% Sample posative patches along false positive boundaries
                for ii = 1 : nsampPos
                    %$% No Translation
                    for tx = 0 %-dx : dx: dx % translation x
                        for ty = 0 %-dy : dy: dy
                            trainPatchMask = realInstMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
                            trainPatchGTMask = realInstGTMask( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR));
                            % ovlpGT = length(find(trainPatchMask .* trainPatchGTMask)) / length( find(trainPatchMask + trainPatchGTMask));
                            ovlpGT = fbIntUni(trainPatchMask, trainPatchGTMask);
                            if ovlpGT >= posOvlpThrsh % 0.8
                                patchGTLabel = 1;
                            elseif ovlpGT <= negOvlpThrsh % 0.3
                                patchGTLabel = 0;
                            else
                                continue;
                            end;
                            if countPatchNum
                                if patchGTLabel == 1
                                    totalPosPatchNum = totalPosPatchNum + 1; % one more positive patch from real segmentation
                                    if mod(totalPosPatchNum,10)==0
                                        disp(['Positive ' num2str(totalPosPatchNum) ' Data']);end;
                                else
                                    totalNegPatchNum = totalNegPatchNum + 1;
                                    if mod(totalNegPatchNum, 100)==0
                                        disp(['Negative ' num2str(totalNegPatchNum) ' Data']); end;
                                end;
                                continue; % only count, not write to dataset
                            end;
                            trainPatchImg = orgInstImg( (yre(ii)+ty-winR):(yre(ii)+ty+winR), (xre(ii)+tx-winR):(xre(ii)+tx+winR), : );
                            trainPatchImg(:,:,end+1) = trainPatchMask;
                            %% Write to HDF5
                            if patchGTLabel == 1
                                curPosPatchIdx = curPosPatchIdx + 1;
                                idxPosInH5 = find(posRandIdx == curPosPatchIdx);
                                h5write(trainPosDatasetFileName,'/data',single(trainPatchImg), [1 1 1 idxPosInH5], [size(trainPatchImg) 1]);
                                h5write(trainPosDatasetFileName,'/label',posLabel, idxPosInH5, 1);
                                h5write(trainPosDatasetFileName,'/sample_weight',posSampleWeight, idxPosInH5, 1);
                            elseif patchGTLabel == 0
                                curNegPatchIdx = curNegPatchIdx + 1;
                                idxNegInH5 = find(negRandIdx == curNegPatchIdx);
                                h5write(trainNegDatasetFileName,'/data',single(trainPatchImg), [1 1 1 idxNegInH5], [size(trainPatchImg) 1]);
                                h5write(trainNegDatasetFileName,'/label',negLabel, idxNegInH5, 1);
                                h5write(trainNegDatasetFileName,'/sample_weight',negSampleWeight, idxNegInH5, 1);
                            end;
                        end; % ty
                    end; % tx
                end;
            end; % rii
            rrTime = toc;
            disp([imgRealSegList(rr).name ' takes ' num2str(rrTime) 's']);
            %             end;  % rr
        end; % mm
        if  countPatchNum
            save(posCountFileName,'totalPosPatchNum','predClsRealSegNum');
            save(negCountFileName,'totalNegPatchNum','predClsRealSegNum');
        end;
    end; % for  countPatchNum
end; % ww
%% OLD:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% get windows around sample points
for ww = 1 : length(winSizeList)
    subwinSize = winSizeList(ww); % sxs window around sample points
    %     hdfPosPath = sprintf('%spos_batch_%dx%d/',hdfPath,subwinSize,subwinSize);if ~isdir(hdfPosPath) mkdir(hdfPosPath); end;
    %     hdfNegPath = sprintf('%sneg_batch_%dx%d/',hdfPath,subwinSize,subwinSize);if ~isdir(hdfNegPath) mkdir(hdfNegPath); end;
    disp(['---- Window Size ' num2str(subwinSize)]);
    winR = floor(subwinSize/2);
    %% Count Negative patche number
    negCountFileName = [sampleDataPath 'negTestCount.mat'];
    if countNegSample == 1
        predClassMaskNum = 0;
        curNegPatchNum = 0;
        load([sampleDataPath savePosTrainPreFileName],'nsampPos','maskW','maskH');
        nsampNeg = nsampPos * 2;
        meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
        refMaskSize = [meanMH meanMW];
        for mm = 1 : valImgNum
            imgFullName = [dsInfo.imdir valList{mm} '.' dsInfo.extension];
            [imgDir imgName imgFormat] = fileparts(imgFullName);
            orgImage = im2double(imread(imgFullName));
            imgH = size(orgImage,1); imgW = size(orgImage,2);
            switch dsInfo.dataset
                case 'sbd'
                    load([sbdInstPath imgName '.mat'],'GTinst');
                    load([sbdClsPath imgName '.mat'],'GTcls');
                    Sclass = GTcls.Segmentation;
                    Sobj = GTinst.Segmentation;
                    objInstIdx = unique(Sobj);
                    objInstIdx = setdiff(objInstIdx, 0);%
                    objInstIdx = setdiff(objInstIdx, 255);
                    imgClassIdxList = GTinst.Categories;
                    if ~ismember(targetClassLabel,imgClassIdxList) % not the target class
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
                case 'voc12'
                    rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
                    imgClassList = {rec.objects.class};
                    objInstIdx = find(strcmp(imgClassList,targetClassName));
                    if length(objInstIdx)
                        [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                        [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
                    else
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
            end;
            imgRealSegFolder = [realSegPath imgName fs];
            imgRealSegList = dir([imgRealSegFolder '*.png']);
            targetClassGTMask = double(Sclass == targetClassLabel);
            for rr = 1 : length(imgRealSegList)
                fprintf('%s Starts\n',[imgRealSegList(rr).name]);
                [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
                realClsIdx = setdiff(unique(realSegImg),[0 255]);
                for rii = 1 : length(realClsIdx)
                    if realClsIdx(rii) ~= targetClassLabel
                        %disp(['Instance is not ' targetClassName ', continue!']);
                        continue;
                    end;
                    predClassMaskNum = predClassMaskNum + 1;
                    realInstMask = double(realSegImg == realClsIdx(rii));
                    [r c v] = find(realInstMask);
                    if length(find(realInstMask)) < minMaskPixThrsh
                        disp('Mask Too Small, Dump!'); continue; end;
                    bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
                    orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = imresize(realInstMask,refMaskSize);
                    orgInstImg = imresize(orgInstImg,refMaskSize);
                    realInstGTMask = imresize(realInstGTMask,refMaskSize);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = padarray(realInstMask,[padr padr]);
                    orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
                    realInstGTMask = padarray(realInstGTMask,[padr padr]);
                    %%%%%%%%%%%%%%%%%%%%%
                    %% sample points for patches
                    [xre,yre,tre]=bdry_extract_3(realInstMask);
                    nsamp2=length(xre);
                    if nsamp2>=nsampNeg
                        [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampNeg);
                    end
                    xre = round(xre); yre = round(yre);
                    %%% No need to padd: points are sampled on padded masks
                    %xre = xre + padr; yre = yre + padr;
                    %negData.points = [negData.points; realPoints];
                    %% Sample negative patches along false positive boundaries
                    for ii = 1 : nsampNeg
                        %$% No Translation
                        testPatchMask = realInstMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        testPatchGTMask = realInstGTMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        ovlpGT = length(find(testPatchMask .* testPatchGTMask)) / length( find(testPatchMask + testPatchGTMask));
                        if ovlpGT > 0.3
                            continue;
                        end;
                        curNegPatchNum = curNegPatchNum + 1;                        
                    end;
                    disp(['Processed ' num2str(curNegPatchNum) ' Data']);
                end; % rii
            end;  % rr
        end; % mm
        disp(['Total predicted ' targetClassName ' Instances:' num2str(predClassMaskNum)]);
        disp(['Total False Positive Patches: ' num2str(curNegPatchNum)]);
        totalNegPatchNum = curNegPatchNum;
        save(negCountFileName,'predClassMaskNum','totalNegPatchNum');
    else
        load(negCountFileName,'predClassMaskNum','totalNegPatchNum');
    end; % if regetNegTestData
    %% Get Data Index
    %saveTestPosPrefix = sprintf('%s%s_pos_%dx%d',hdfPosPath,saveTestPrefix,subwinSize,subwinSize);
    %saveTestNegPrefix = sprintf('%s%s_neg_%dx%d',hdfNegPath,saveTestPrefix,subwinSize,subwinSize);
    %     totalPosPatchNum = testPosInstNum * nsampPos * numel([-dx : dx: dx]) * numel([-dy : dy: dy]);
    %     totalPatchNum = totalPosPatchNum + totalNegPatchNum;
    %     posRandIdx = randperm(totalPosPatchNum);
    negRandIdx = randperm(totalNegPatchNum);%+totalPosPatchNum;
    %     caffeBatchSize = 100; caffeBatchNum = ceil(totalPatchNum / caffeBatchSize);
    %     posNum = floor(caffeBatchSize * totalPosPatchNum/totalPatchNum);
    %     negNum = caffeBatchSize - posNum;
    %     allRandIdx = [];
    %     negBatchBeginNum = 1;
    %     for bid = 1 : caffeBatchNum
    %         posBatchBeginNum = posNum * (bid-1) + 1;
    %         posBatchEndNum = min(posNum * bid, totalPosPatchNum);
    %         curBatchPosNum = posBatchEndNum - posBatchBeginNum + 1;
    %         negBatchEndNum = min(negBatchBeginNum + (caffeBatchSize - curBatchPosNum)-1,totalNegPatchNum);
    %         curBatchNegNum = negBatchEndNum - negBatchBeginNum + 1;
    %         curBatchSize = curBatchPosNum + curBatchNegNum;
    %         curBatchIdx = [posRandIdx(posBatchBeginNum:posBatchEndNum) negRandIdx(negBatchBeginNum:negBatchEndNum)];
    %         curBatchIdx = curBatchIdx(randperm(curBatchSize));
    %         allRandIdx = [allRandIdx curBatchIdx];
    %         negBatchBeginNum = negBatchBeginNum + curBatchNegNum;
    %     end; % bid
    %% Create HDF5 dataset
    testNegDatasetFileName = sprintf('%stestNegPatches_%dx%d.h5',posNegPatchPath, subwinSize, subwinSize);
    if ~exist(testNegDatasetFileName,'file')
        h5create(testNegDatasetFileName,'/data',[subwinSize subwinSize 4 Inf],'Datatype','single','ChunkSize',[subwinSize subwinSize 1 1]);
        h5create(testNegDatasetFileName,'/label',Inf,'Datatype','single','ChunkSize',1);
        h5create(testNegDatasetFileName,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);
    end;
    % posSampleWeight = single(1);
    %% Test by Stephen 1/29/2015
    negSampleWeight = single(5/95/10);%single(totalPosPatchNum / (10 * totalNegPatchNum));
    %%
    % negSampleWeight = single(totalPosPatchNum / totalNegPatchNum);
    posLabel = single(1);
    negLabel = single(0);
    curPatchIdx = 0;
    %% Negative data
    visited = 0;
    debugFlag = 0;
    if regetNegTestData == 1
        load([sampleDataPath savePosTrainPreFileName],'nsampPos','maskW','maskH');
        nsampNeg = nsampPos * 2;
        meanMW = round(mean(maskW)); meanMH = round(mean(maskH));
        refMaskSize = [meanMH meanMW];
        for mm = 1 : valImgNum
            imgFullName = [dsInfo.imdir valList{mm} '.' dsInfo.extension];
            [imgDir imgName imgFormat] = fileparts(imgFullName);
            %% debug
            if debugFlag
                debugImgName = '2011_002993';
                if ~strcmp(imgName,debugImgName) && visited == 0
                    continue;
                elseif strcmp(imgName,debugImgName)
                    visited = 1;
                    curPatchIdx = 1986024;
                end;
            end;
            %%
            orgImage = im2double(imread(imgFullName));
            imgH = size(orgImage,1); imgW = size(orgImage,2);
            switch dsInfo.dataset
                case 'sbd'
                    load([sbdInstPath imgName '.mat'],'GTinst');
                    load([sbdClsPath imgName '.mat'],'GTcls');
                    Sclass = GTcls.Segmentation;
                    Sobj = GTinst.Segmentation;
                    objInstIdx = unique(Sobj);
                    objInstIdx = setdiff(objInstIdx, 0);%
                    objInstIdx = setdiff(objInstIdx, 255);
                    imgClassIdxList = GTinst.Categories;
                    if ~ismember(targetClassLabel,imgClassIdxList) % not the target class
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
                case 'voc12'
                    rec=PASreadrecord(sprintf(VOCopts.annopath,imgName));
                    imgClassList = {rec.objects.class};
                    objInstIdx = find(strcmp(imgClassList,targetClassName));
                    if length(objInstIdx)
                        [Sobj,CMobj]=imread(sprintf(VOCopts.seg.instimgpath,imgName));
                        [Sclass,CMclass]=imread(sprintf(VOCopts.seg.clsimgpath,imgName));
                    else
                        disp(['Image has no ' targetClassName ', continue!']);
                        continue;
                    end;
            end;
            imgRealSegFolder = [realSegPath imgName fs];
            imgRealSegList = dir([imgRealSegFolder '*.png']);
            targetClassGTMask = double(Sclass == targetClassLabel);
            for rr = 1 : length(imgRealSegList)
                %%
                if debugFlag && visited
                    if ~strcmp(imgRealSegList(rr).name,[debugImgName '_S0030.png'])
                        continue;
                    else
                        curPatchIdx = 269839; end;
                end;
                %%
                tic;
                fprintf('%s Starts\n',[imgRealSegList(rr).name]);
                [realSegImg,CMreal]=imread([imgRealSegFolder imgRealSegList(rr).name]);
                realClsIdx = setdiff(unique(realSegImg),[0 255]);
                for rii = 1 : length(realClsIdx)
                    if realClsIdx(rii) ~= targetClassLabel
                        %disp(['Instance is not ' targetClassName ', continue!']);
                        continue;
                    end;
                    realInstMask = double(realSegImg == realClsIdx(rii));
                    [r c v] = find(realInstMask);
                    if length(find(realInstMask)) < minMaskPixThrsh
                        disp('Mask Too Small, Dump!'); continue; end;
                    bbox = [max(min(c)-10,1)  max(min(r)-10,1) min(max(c)+10,imgW) min(max(r)+10,imgH)];
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = realInstMask(bbox(2):bbox(4),bbox(1):bbox(3)); % bbox: [x1 y1 x2 y2];[y1:y2 x1:x2]
                    orgInstImg = orgImage(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    realInstGTMask = targetClassGTMask(bbox(2):bbox(4),bbox(1):bbox(3),:);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = imresize(realInstMask,refMaskSize);
                    orgInstImg = imresize(orgInstImg,refMaskSize);
                    realInstGTMask = imresize(realInstGTMask,refMaskSize);
                    %%%%%%%%%%%%%%%%%%%%%
                    realInstMask = padarray(realInstMask,[padr padr]);
                    orgInstImg = padarray(orgInstImg,[padr padr],'symmetric');
                    realInstGTMask = padarray(realInstGTMask,[padr padr]);
                    %%%%%%%%%%%%%%%%%%%%%
                    %% sample points for patches
                    [xre,yre,tre]=bdry_extract_3(realInstMask);
                    nsamp2=length(xre);
                    if nsamp2>=nsampNeg
                        [xre,yre,tre]=get_samples_1(xre,yre,tre,nsampNeg);
                    end
                    xre = round(xre); yre = round(yre);
                    %%% No need to padd: points are sampled on padded masks
                    %xre = xre + padr; yre = yre + padr;
                    %negData.points = [negData.points; realPoints];
                    %% Sample negative patches along false positive boundaries
                    for ii = 1 : nsampNeg
                        %$% No Translation
                        testPatchMask = realInstMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        testPatchGTMask = realInstGTMask( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR));
                        ovlpGT = length(find(testPatchMask .* testPatchGTMask)) / length( find(testPatchMask + testPatchGTMask));
                        if ovlpGT > 0.3
                            continue;
                        end;
                        testPatchImg = orgInstImg( (yre(ii)-winR):(yre(ii)+winR), (xre(ii)-winR):(xre(ii)+winR), : );
                        testPatchImg(:,:,end+1) = testPatchMask;
                        %% Write to HDF5
                        curPatchIdx = curPatchIdx + 1;
                        idxInH5 = find(negRandIdx == curPatchIdx);
                        if isempty(idxInH5) % out of limit
                            info = hdf5info(testNegDatasetFileName);
                            idxInH5 = info.GroupHierarchy.Datasets(1).Dims(4) +1;
                        end;
                        h5write(testNegDatasetFileName,'/data',single(testPatchImg), [1 1 1 idxInH5], [size(testPatchImg) 1]);
                        h5write(testNegDatasetFileName,'/label',negLabel, idxInH5, 1);
                        h5write(testNegDatasetFileName,'/sample_weight',negSampleWeight, idxInH5, 1);
                    end;
                    disp(['Processed ' num2str(curPatchIdx) ' Data']);
                end; % rii
                rrTime = toc;
                disp([imgRealSegList(rr).name ' takes ' num2str(rrTime) 's']);
            end;  % rr
        end; % mm
    end; % if regetNegTestData    
end; % ww
return;


%%
function [resBbox] = getResBbox(img, newSize,pixValThrsh)
[r c v] = find(img);
resBbox = img(min(r):max(r),min(c):max(c));
resBbox = imresize(resBbox, newSize);
resBbox = double(resBbox > pixValThrsh);