function [] = analyzeTraining()
close all;
fileFolder = '//share/project/shapes/caffe-weighted-samples/examples/caffe_log_files/'; 
filePrefix = 'caffe.poincare.tti-c.org.stephenchen.log.INFO.';
[fileName] = getLatestFile(fileFolder, filePrefix);
logFileName = [fileFolder fileName];
% fileName = '/tmp/caffe.poincare.tti-c.org.stephenchen.log.INFO.20150215-211051.22948';
logStr = textread(logFileName,'%s','delimiter','\n');
trainDisplayIter = strValSearch(logStr, 'display:%d');
iterIdx = strSearch(logStr, 'Train net output #0:');
trainDispNum = length(iterIdx)-1;
trainIter = trainDisplayIter * (trainDispNum); % minus iteration 0
%%%
testInterval = strValSearch(logStr,'test_interval:%d');
%%%
plotTrainIter = trainDisplayIter; % {trainDisplayIter,testInterval};
trainDispPerTest = plotTrainIter / trainDisplayIter;
%%
trainLossOrg = strValSearch(logStr,'Train net output #0: loss = %f');
trainLoss = zeros(size(trainLossOrg));
trLossWinSize = 100; % average the train loss in the window
for tt = 1 : length(trainLossOrg)
    trainLoss(tt) = mean(trainLossOrg(max(1,tt-trLossWinSize):tt));
end;    

testAcc = strValSearch(logStr,'Test net output #0: accuracy = %f'); % {'Test net output #0: accuracy = %f'}
testLoss = strValSearch(logStr,'Test net output #1: loss = %f'); % {'Test net output #1: loss = %f'};
% testAcc = testAcc(2:end); testLoss = testLoss(2:end); % remove test result at iter 0 (random)



figure,hold on;
%plot([0:trainDisplayIter:trainIter],trainLoss,'r');
plot([0:trainDisplayIter:trainIter],trainLoss(1:trainDispPerTest:end),'r');
plot([0:testInterval:testInterval*(length(testLoss)-1)], testLoss,'b');
plot([0:testInterval:testInterval*(length(testAcc)-1)], 1-testAcc,'g');
legend('Train Loss','Test Loss','Classification Error');
title('Train/Test Loss and Classification Error Variation during Training');
xlabel('Loss and Classification Error');
ylabel('Loss and Classification Error');
xlabel('Iterations');
set(gca,'YGrid', 'on');
set(gca,'YTick',[0:0.1:2])
hold off



function [fileName] = getLatestFile(fileFolder, filePrefix)
fileList = dir([fileFolder filePrefix '*']);
dates = [fileList.datenum];
[val, idx] = max(dates);
fileName = fileList(idx).name;