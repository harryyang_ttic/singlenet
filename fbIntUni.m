function [ovlp] = fbIntUni(img1, img2) 
% def: fore/background intersect/union: = (f^f'/fUf' + b^b'/bUb')/2
inter = img1.*img2;
uni = double(img1+img2);
fgivu = length(find(inter))/length(find(img1+img2));
bgivu = length(find(~uni)) / length(find(~inter));
ovlp = ( fgivu + bgivu ) /2;
debugFlag = 0;
if debugFlag && fgivu
    subaxisParam = {'Spacing', 0.05, 'Padding', 0, 'Margin', 0.05};
    %%
    subaxis(2,3,1, subaxisParam{:});
    axis tight;axis off;
    imshow(img1);
    hold on;title(['A (fg int/uni = ' num2str(fgivu) ')']); hold off
    %%
    subaxis(2,3,4, subaxisParam{:});
    axis tight;axis off;
    imshow(img2);
    hold on;title(['B (bg int/uni = ' num2str(bgivu) ')']); hold off
    %%
    subaxis(2,3,2, subaxisParam{:});
    axis tight;axis off;
    imshow(inter);
    hold on;title('A\capB'); hold off
    %% Hypothesized masks
    subaxis(2,3,3, subaxisParam{:});
    axis tight;axis off;
    imshow(0.3*img1+0.7*img2);
    hold on;title('A\cupB'); hold off
    %% Groundtruth
    subaxis(2,3,5, subaxisParam{:});
    axis tight;axis off;
    imshow(~uni);    
    hold on;title('~A\cap~B'); hold off
    %% Groundtruth
    subaxis(2,3,6, subaxisParam{:});
    axis tight;axis off;
    imshow(~inter);    
    hold on;title('~A\cup~B'); hold off    
end;