function [fig] = visBoundaryCls(orgImage, instMask, gtMask,...
    batchPatchMasks, points, clsScores, ovlpGT, titleStrs,varargin)
%% Parse Input Arguments
for pair = reshape(varargin{:},2,[])
    tmp = pair{2};
    eval([pair{1} ' = tmp;']);
    clear tmp;
end; %pair
colorBarFlag = 0;
%%
if colorBarFlag
    subaxisParam = {'Spacing', 0.01, 'Padding', 0, 'Margin',0,'MarginRight', 0.15 };
else subaxisParam = {'Spacing', 0.01, 'Padding', 0, 'Margin',0};
end;
if exist('img1','var') && exist('img2','var')
    plotrow = 2;
else
    plotrow = 1;
end;
%%
nsamps = size(points,1);
ws = size(batchPatchMasks{1},1);
wr = ws/2;
%% calculate the average ovlp
avgOvlp = avgNearScores(points, wr, ovlpGT);
%%
xre = points(:,1); yre = points(:,2);
posIdx = find(avgOvlp >= 0.5);
negIdx = find(avgOvlp < 0.5);
ovlpGTLabels(posIdx) = 1;
ovlpGTLabels(negIdx) = 0;
clsLabels = (clsScores > 0.5)';
truePosId = find(clsLabels == 1 & ovlpGTLabels == 1);
trueNegId = find(clsLabels == 0 & ovlpGTLabels == 0);
falsePosId = find(clsLabels == 1 & ovlpGTLabels == 0);
falseNegId = find(clsLabels == 0 & ovlpGTLabels == 1);
evalLabel = zeros(size(clsLabels));
evalLabel(truePosId) = 1; evalLabel(trueNegId) = 0;
evalLabel(falsePosId) = 2; evalLabel(falseNegId) = 3;
% eval(posIdx) = (clsLabels(posIdx) == ovlpGTLabels(posIdx));
% eval(negIdx) = (clsLabels(negIdx) == ovlpGTLabels(negIdx));
%%
tic;
subaxis(2,2,1,subaxisParam{:});
axis tight
axis off
imshow(orgImage);
hold on; 
if exist('titleStrs','var') && length(titleStrs{1}) > 0 title(titleStrs{1}); 
else title('original'); end; hold off
%toc; tic;
%% Groundtruth
h2 = subaxis(2,2,2,subaxisParam{:});
% colormap(s3, hot(8));
axis tight
axis off
imshow(gtMask*0.8+instMask*0.2);
if colorBarFlag
    originalSize2 = get(gca, 'Position');
    hCbar2 = colorbar;
    set(h2, 'Position', originalSize2); % Can also use gca instead of h1 if h1 is still active.
end;
boundaryPtsBox(xre, yre, ws, ovlpGTLabels);
if exist('titleStrs','var') && length(titleStrs{3}) > 0 title(titleStrs{3}); 
else title('GT Mask'); end; hold off
%toc; tic;
%% Hypothesized masks
subaxis(2,2,3,subaxisParam{:});
axis tight
axis off
imshow(instMask);
boundaryPtsBox(xre, yre, ws, ovlpGTLabels);
if exist('titleStrs','var') && length(titleStrs{2}) > 0 title(titleStrs{2}); 
else title('Hypo Mask'); end; hold off
%toc; tic;
%% Showing annotation
%imshow(instMask);
h4 = subaxis(2,2,4,subaxisParam{:});
axis tight
axis off
wr = floor(ws/2);
hold on;
imshow(color_seg_rgb(instMask, orgImage));
%%%%%
if colorBarFlag
    originalSize4 = get(gca, 'Position');
    hCbar4 = colorbar;
    set(h4, 'Position', originalSize4);
end;
%%%%
boundaryPtsBox(xre, yre, ws, evalLabel);%,1 );
colormap default;
curmap = colormap; 
curmap = [0 0 0; curmap]; 
%curmap = [curmap; 1 1 1];
colormap(curmap);
%%%%
if colorBarFlag
    set(hCbar2,'YTick',[0 0.2 0.8 1]);
    set(hCbar2,'YTickLabel',{'TrueBG','FalsePos','FalseNeg','TruePos'});
    %%%%
    set(hCbar4,'YTick',[0:0.1:1]);
end;
%%%%%
a = 60;
nsamp = length(xre);
cmIds = round(linspace(1, size(curmap,1), nsamp));
[val sortedInOrg] = sort(avgOvlp,'ascend');
[val rank] = sort(sortedInOrg,'ascend');
hold on; 
scatter(xre,yre,a,curmap(cmIds(rank),:),'filled'); hold on;
%%%%%
if exist('titleStrs','var') && length(titleStrs{4}) > 0 title(titleStrs{4}); 
else title('Heat Map'); end; hold off
hold off
toc; 
%% save image
if exist('saveImgName','var')
    set(gcf,'outerposition', get(0,'screensize'));
    saveas(gcf, saveImgName);
end;







%%
function [avgScores] = avgNearScores(pts, radius, orgScores)
dists = pdist2(pts, pts);
avgScores = zeros(size(orgScores));
for pp = 1 : size(pts,1)
    ptDist = dists(pp,:);
    nnIds = find(ptDist <= radius);
    avgScores(pp) = mean(orgScores(nnIds));
end;



%%
%saveas(gcf, [saveVisFolder imgName '.png']);
function [] = boundaryPtsBox(xre, yre, ws, colorMap,legendFlag)
if ~exist('legendFlag','var') legendFlag = 0; end;
nsamps = length(xre); 
wr = ws/2;
handles = cell(1,4);
visited = zeros(1,4);
for ii = 1 : nsamps
    px = xre(ii); py = yre(ii);
    switch colorMap(ii)
        case 1 % true positive
            boxColor = 'g';
        case 0 % true negative
            boxColor = 'r';
        case 2 % false positive
            boxColor = 'y';
        case 3 % false negative
            boxColor = 'b';
        otherwise
            boxColor = 'm';
    end;
    hold on;
    h1 = rectangle('Position',[px-wr py-wr ws ws],'EdgeColor',boxColor); 
    if legendFlag && visited(colorMap(ii)+1) == 0
        p1=plot(nan,nan,'s','markeredgecolor',get(h1,'edgecolor'),...
            'markerfacecolor',get(h1,'facecolor'));
        handles{colorMap(ii)+1} = p1;
        visited(colorMap(ii)+1) = 1;
    end;
    % plot(px,py,'r*');
    axis image;
end; %ii
if legendFlag
    legend(cell2mat(handles),{'true neg','true pos','false pos','false neg'},'Location','southeast')
end;

