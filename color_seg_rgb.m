function im1=color_seg_rgb(seg, img, color)
img=im2double(img);
im1=zeros(size(img));
if(~exist('color', 'var'))
	color=[0 1 1];
end
im1(:,:,1)=0.5*color(1)*seg+0.5*img(:,:,1);
im1(:,:,2)=0.5*color(2)*seg+0.5*img(:,:,2);
im1(:,:,3)=0.5*color(3)*seg+0.5*img(:,:,3);